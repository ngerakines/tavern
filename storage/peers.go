package storage

import (
	"context"

	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/errors"
)

type PeerStorage interface {
	CreatePeer(ctx context.Context, peerID uuid.UUID, inbox string) error
}

func (s pgStorage) CreatePeer(ctx context.Context, peerID uuid.UUID, inbox string) error {
	now := s.now()
	_, err := s.db.ExecContext(ctx, "INSERT INTO peers (id, inbox, created_at) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING", peerID, inbox, now)
	return errors.WrapInsertQueryFailedError(err)
}
