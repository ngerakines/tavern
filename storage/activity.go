package storage

func ActivityDestinations(activity Payload) []string {
	var results []string

	locations, ok := JSONStrings(activity, "to")
	if ok {
		for _, location := range locations {
			results = append(results, location)
		}
	}
	locations, ok = JSONStrings(activity, "cc")
	if ok {
		for _, location := range locations {
			results = append(results, location)
		}
	}

	object, ok := JSONMap(activity, "object")
	if ok {
		locations, ok = JSONStrings(object, "to")
		if ok {
			for _, location := range locations {
				results = append(results, location)
			}
		}
		locations, ok = JSONStrings(object, "cc")
		if ok {
			for _, location := range locations {
				results = append(results, location)
			}
		}
	}

	return results
}
