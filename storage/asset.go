package storage

import (
	"context"
	"fmt"
	"strings"

	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/errors"
)

type AssetStorage interface {
	CreateImage(ctx context.Context, location, checksum, blur string, size, contentType, height, width int, aliases []string) (ImageAsset, error)
	GetImagesByLocation(ctx context.Context, locations []string) ([]ImageAsset, error)
	GetImagesByAlias(ctx context.Context, aliases []string) ([]ImageAsset, error)
	GetImageByChecksum(ctx context.Context, checksum string) (ImageAsset, error)
}

type ImageAsset struct {
	ID          uuid.UUID
	Location    string
	Checksum    string
	Blur        string
	Size        int
	ContentType int
	Height      int
	Width       int
	Alias       string
}

func (ia ImageAsset) GetContentType() string {
	switch ia.ContentType {
	case ContentTypeJPG:
		return "image/jpeg"
	case ContentTypePNG:
		return "image/png"
	default:
		return ""
	}
}

var _ AssetStorage = &pgStorage{}

const (
	ContentTypeUnknown = 0
	ContentTypePNG     = 1
	ContentTypeJPG     = 2
	ContentTypeSVG     = 3
)

func (s pgStorage) CreateImage(ctx context.Context, location, checksum, blur string, size, contentType, height, width int, aliases []string) (ImageAsset, error) {
	var img ImageAsset
	now := s.now()
	txErr := runTransactionWithOptions(s.db, func(tx QueryExecute) error {
		checksumCount, err := s.rowCount(ctx, `SELECT COUNT(*) FROM images WHERE checksum = $1`, checksum)
		if err != nil {
			return errors.WrapImageQueryFailedError(err)
		}
		if checksumCount > 0 {
			images, err := s.getImagesQuery(tx, ctx, `SELECT id, location, checksum, blur, size, content_type, height, width, '' FROM images WHERE checksum = $1`, checksum)
			if err != nil {
				return err
			}
			if len(images) == 0 {
				return errors.WrapImageQueryFailedError(fmt.Errorf("count returned at least one but no results found"))
			}
			img = images[0]
			return nil
		}

		imageRowID := NewV4()
		_, err = tx.ExecContext(ctx, `INSERT INTO images (id, location, checksum, content_type, blur, height, width, "size", created_at) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`, imageRowID, location, checksum, contentType, blur, height, width, size, now)
		if err != nil {
			return errors.WrapImageInsertFailedError(err)
		}
		for _, alias := range aliases {
			_, err = tx.ExecContext(ctx, `INSERT INTO image_aliases (id, image_id, alias, created_at) VALUES ($1, $2, $3, $4)`, NewV4(), imageRowID, alias, now)
			if err != nil {
				return errors.WrapImageAliasInsertFailedError(err)
			}
		}
		img, err = s.getImage(tx, ctx, imageRowID)
		return err
	})
	if txErr != nil {
		return ImageAsset{}, txErr
	}
	return img, nil
}

func (s pgStorage) GetImage(ctx context.Context, id uuid.UUID) (ImageAsset, error) {
	images, err := s.getImagesQuery(s.db, ctx, `SELECT id, location, checksum, blur, size, content_type, height, width, '' FROM images WHERE id = $1`, id)
	if err != nil {
		return ImageAsset{}, err
	}
	if len(images) == 0 {
		return ImageAsset{}, errors.NewImageNotFoundError(nil)
	}
	return images[0], nil
}

func (s pgStorage) GetImageByChecksum(ctx context.Context, checksum string) (ImageAsset, error) {
	images, err := s.getImagesQuery(s.db, ctx, `SELECT id, location, checksum, blur, size, content_type, height, width, '' FROM images WHERE checksum = $1`, checksum)
	if err != nil {
		return ImageAsset{}, err
	}
	if len(images) == 0 {
		return ImageAsset{}, errors.NewImageNotFoundError(nil)
	}
	return images[0], nil
}

func (s pgStorage) getImage(qc QueryExecute, ctx context.Context, id uuid.UUID) (ImageAsset, error) {
	images, err := s.getImagesQuery(qc, ctx, `SELECT id, location, checksum, blur, size, content_type, height, width, '' FROM images WHERE id = $1`, id)
	if err != nil {
		return ImageAsset{}, err
	}
	if len(images) == 0 {
		return ImageAsset{}, errors.NewImageNotFoundError(nil)
	}
	return images[0], nil
}

func (s pgStorage) GetImagesByLocation(ctx context.Context, locations []string) ([]ImageAsset, error) {
	if len(locations) == 0 {
		return nil, nil
	}
	params := make([]string, len(locations))
	args := make([]interface{}, len(locations))
	for i, id := range locations {
		params[i] = fmt.Sprintf("$%d", i+1)
		args[i] = id
	}

	query := fmt.Sprintf(`SELECT id, location, checksum, blur, size, content_type, height, width, '' FROM images WHERE location IN (%s)`, strings.Join(params, ", "))
	return s.getImagesQuery(s.db, ctx, query, args...)
}

func (s pgStorage) GetImagesByAlias(ctx context.Context, aliases []string) ([]ImageAsset, error) {
	if len(aliases) == 0 {
		return nil, nil
	}
	params := make([]string, len(aliases))
	args := make([]interface{}, len(aliases))
	for i, id := range aliases {
		params[i] = fmt.Sprintf("$%d", i+1)
		args[i] = id
	}

	query := fmt.Sprintf(`SELECT im.id, im.location, im.checksum, im.blur, im.size, im.content_type, im.height, im.width, alias FROM images im INNER JOIN image_aliases ima ON im.id = ima.image_id WHERE ima.alias IN (%s)`, strings.Join(params, ", "))
	return s.getImagesQuery(s.db, ctx, query, args...)
}

func (s pgStorage) getImagesQuery(qc QueryExecute, ctx context.Context, query string, args ...interface{}) ([]ImageAsset, error) {
	var results []ImageAsset

	rows, err := qc.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, errors.NewSelectQueryFailedError(err)
	}
	defer rows.Close()
	for rows.Next() {
		var imageAsset ImageAsset
		if err := rows.Scan(
			&imageAsset.ID,
			&imageAsset.Location,
			&imageAsset.Checksum,
			&imageAsset.Blur,
			&imageAsset.Size,
			&imageAsset.ContentType,
			&imageAsset.Height,
			&imageAsset.Width,
			&imageAsset.Alias,
		); err != nil {
			return nil, errors.NewImageSelectFailedError(err)
		}
		results = append(results, imageAsset)
	}

	return results, nil
}
