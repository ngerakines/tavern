package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJSONString(t *testing.T) {
	doc := map[string]interface{}{
		"foo": 1,
		"bar": "a",
		"baz": map[string]interface{}{
			"foo": "b",
		},
	}
	{
		value, ok := JSONString(doc, "foo")
		assert.Equal(t, "", value)
		assert.False(t, ok)
	}
	{
		value, ok := JSONString(doc, "bar")
		assert.Equal(t, "a", value)
		assert.True(t, ok)
	}
	{
		value, ok := JSONString(doc, "baz")
		assert.Equal(t, "", value)
		assert.False(t, ok)
	}
}

func TestJSONDeepString(t *testing.T) {
	doc := map[string]interface{}{
		"foo": 1,
		"bar": "a",
		"baz": map[string]interface{}{
			"foo": "b",
		},
	}
	{
		value, ok := JSONDeepString(doc, "foo")
		assert.Equal(t, "", value)
		assert.False(t, ok)
	}
	{
		value, ok := JSONDeepString(doc, "bar")
		assert.Equal(t, "a", value)
		assert.True(t, ok)
	}
	{
		value, ok := JSONDeepString(doc, "baz", "foo")
		assert.Equal(t, "b", value)
		assert.True(t, ok)
	}
}
