package storage

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/errors"
)

func userIDFromSession(session sessions.Session) (uuid.UUID, error) {
	return userIDFromSessionKey(session, gin.AuthUserKey)
}

func userIDFromSessionKey(session sessions.Session, key string) (uuid.UUID, error) {
	sessionData := session.Get(key)
	if sessionData == nil {
		return uuid.Nil, errors.NewUserSessionNotFoundError(nil)
	}
	userIDStr, ok := sessionData.(string)
	if !ok {
		return uuid.Nil, errors.NewInvalidUserIDError(nil)
	}
	userID, err := uuid.FromString(userIDStr)
	if !ok {
		return uuid.Nil, errors.NewInvalidUserIDError(err)
	}
	return userID, nil
}
