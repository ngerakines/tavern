package storage

import (
	"github.com/ngerakines/tavern/common"
)

func JSONDeepString(document map[string]interface{}, keys ...string) (string, bool) {
	if len(keys) == 1 {
		return JSONString(document, keys[0])
	}
	if len(keys) > 1 {
		inner, ok := JSONMap(document, keys[0])
		if ok {
			return JSONDeepString(inner, keys[1:]...)
		}
	}
	return "", false
}

func CollectJSONDeepStrings(document map[string]interface{}, keySets ...[]string) []string {
	results := make([]string, 0)
	for _, keys := range keySets {
		values, _ := JSONDeepStrings(document, keys...)
		results = append(results, values...)
	}
	return common.RemoveTrimmedEmptyStrings(results)
}

func FirstJSONDeepStrings(document map[string]interface{}, keySets ...[]string) string {
	results := make([]string, 0)
	for _, keys := range keySets {
		values, _ := JSONDeepStrings(document, keys...)
		results = append(results, values...)
	}
	values := common.RemoveTrimmedEmptyStrings(results)
	if len(values) == 0 {
		return ""
	}
	return values[0]
}

func JSONDeepStrings(document map[string]interface{}, keys ...string) ([]string, bool) {
	if len(keys) == 1 {
		return JSONStrings(document, keys[0])
	}
	if len(keys) > 1 {
		inner, ok := JSONMap(document, keys[0])
		if ok {
			return JSONDeepStrings(inner, keys[1:]...)
		}
	}
	return []string{}, false
}

func FirstJSONDeepBooleans(document map[string]interface{}, keySets ...[]string) bool {
	results := make([]bool, 0)
	for _, keys := range keySets {
		values, _ := JSONDeepBooleans(document, keys...)
		results = append(results, values...)
	}
	if len(results) == 0 {
		return false
	}
	return results[0]
}

func JSONDeepBooleans(document map[string]interface{}, keys ...string) ([]bool, bool) {
	if len(keys) == 1 {
		return JSONBooleans(document, keys[0])
	}
	if len(keys) > 1 {
		inner, ok := JSONMap(document, keys[0])
		if ok {
			return JSONDeepBooleans(inner, keys[1:]...)
		}
	}
	return []bool{}, false
}

func JSONMap(document map[string]interface{}, key string) (map[string]interface{}, bool) {
	if value, ok := document[key]; ok {
		if mapVal, isMap := value.(map[string]interface{}); isMap {
			return mapVal, true
		}
		if payloadVal, isPayload := value.(Payload); isPayload {
			return payloadVal, true
		}
	}
	return nil, false
}

func JSONString(document map[string]interface{}, key string) (string, bool) {
	if value, ok := document[key]; ok {
		if strValue, isString := value.(string); isString {
			return strValue, true
		}
	}
	return "", false
}

func FirstJSONString(document map[string]interface{}, key string) (string, bool) {
	values, ok := JSONStrings(document, key)
	if len(values) > 0 {
		return values[0], ok
	}
	return "", ok
}

func JSONStrings(document map[string]interface{}, key string) ([]string, bool) {
	results := make([]string, 0)
	value, ok := document[key]
	if !ok {
		return nil, false
	}
	switch v := value.(type) {
	case string:
		results = append(results, v)
	case []interface{}:
		for _, el := range v {
			if strValue, isString := el.(string); isString {
				results = append(results, strValue)
			}
		}
	case []string:
		for _, el := range v {
			results = append(results, el)
		}
	}
	return results, true
}

func JSONBooleans(document map[string]interface{}, key string) ([]bool, bool) {
	results := make([]bool, 0)
	value, ok := document[key]
	if !ok {
		return nil, false
	}
	switch v := value.(type) {
	case bool:
		results = append(results, v)
	case []interface{}:
		for _, el := range v {
			if strValue, isBool := el.(bool); isBool {
				results = append(results, strValue)
			}
		}
	case []bool:
		for _, el := range v {
			results = append(results, el)
		}
	}
	return results, true
}

func JSONMapList(document map[string]interface{}, key string) ([]map[string]interface{}, bool) {
	results := make([]map[string]interface{}, 0)
	value, ok := document[key]
	if !ok {
		return nil, false
	}
	switch v := value.(type) {
	case map[string]interface{}:
		results = append(results, v)
	case Payload:
		results = append(results, v)
	case []interface{}:
		for _, el := range v {
			if mapValue, isMap := el.(map[string]interface{}); isMap {
				results = append(results, mapValue)
			}
		}
	}
	return results, true
}

func StringsContainsString(things []string, value string) bool {
	for _, thing := range things {
		if thing == value {
			return true
		}
	}
	return false
}
