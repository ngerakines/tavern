package storage

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"io"
	"strings"
)

type Payload map[string]interface{}

func (p Payload) Write(w io.Writer) error {
	e := json.NewEncoder(w)
	e.SetEscapeHTML(false)
	return e.Encode(p)
}

func (p Payload) Bytes() []byte {
	var buf bytes.Buffer
	p.Write(&buf)
	return buf.Bytes()
}

func (p Payload) Value() (driver.Value, error) {
	return json.Marshal(p)
}

func (p *Payload) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &p)
}

func EmptyPayload() Payload {
	return make(map[string]interface{})
}

func PayloadFromReader(r io.Reader) (Payload, error) {
	lr := io.LimitReader(r, 1048576)
	decoder := json.NewDecoder(lr)
	var p Payload
	if err := decoder.Decode(&p); err != nil {
		return nil, err
	}
	return p, nil
}

func PayloadFromBytes(b []byte) (Payload, error) {
	return PayloadFromReader(bytes.NewReader(b))
}

func PayloadFromString(s string) (Payload, error) {
	return PayloadFromReader(strings.NewReader(s))
}
