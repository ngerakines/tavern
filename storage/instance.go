package storage

import (
	"context"

	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/errors"
)

type InstanceStatsStorage interface {
	CountUserFollowers(ctx context.Context, userID uuid.UUID) (int, error)
	CountUserFollowing(ctx context.Context, userID uuid.UUID) (int, error)
	CountUsers(ctx context.Context) (int, error)
	CountUsersLastMonth(ctx context.Context) (int, error)
	CountUsersLastHalfYear(ctx context.Context) (int, error)
	CountObjectEvents(ctx context.Context) (int, error)
	CountUserObjectEvents(ctx context.Context, userID uuid.UUID) (int, error)
}

func (s pgStorage) CountUserFollowers(ctx context.Context, userID uuid.UUID) (int, error) {
	return s.wrappedRowCount(errors.WrapNetworkRelationshipQueryFailedError, ctx, `SELECT COUNT(*) FROM network_graph WHERE user_id = $1 AND relationship_type = $2`, userID, UserFollowedByRelationship)
}

func (s pgStorage) CountUserFollowing(ctx context.Context, userID uuid.UUID) (int, error) {
	return s.wrappedRowCount(errors.WrapNetworkRelationshipQueryFailedError, ctx, `SELECT COUNT(*) FROM network_graph WHERE user_id = $1 AND relationship_type = $2`, userID, UserFollowsRelationship)
}

func (s pgStorage) CountUsers(ctx context.Context) (int, error) {
	return s.wrappedRowCount(errors.WrapUserQueryFailedError, ctx, `SELECT COUNT(*) FROM users`)
}

func (s pgStorage) CountUsersLastMonth(ctx context.Context) (int, error) {
	return s.wrappedRowCount(errors.WrapUserQueryFailedError, ctx, `SELECT COUNT(*) FROM users WHERE last_auth_at > NOW() - INTERVAL '31 days'`)
}

func (s pgStorage) CountUsersLastHalfYear(ctx context.Context) (int, error) {
	return s.wrappedRowCount(errors.WrapUserQueryFailedError, ctx, `SELECT COUNT(*) FROM users WHERE last_auth_at > NOW() - INTERVAL '183 days'`)
}

func (s pgStorage) CountObjectEvents(ctx context.Context) (int, error) {
	return s.wrappedRowCount(errors.WrapObjectEventQueryFailedError, ctx, `SELECT COUNT(*) FROM user_object_events`)
}

func (s pgStorage) CountUserObjectEvents(ctx context.Context, userID uuid.UUID) (int, error) {
	return s.wrappedRowCount(errors.WrapUserObjectEventQueryFailedError, ctx, `SELECT COUNT(*) FROM user_object_events WHERE user_id = $1`, userID)
}
