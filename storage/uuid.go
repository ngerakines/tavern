package storage

import (
	"github.com/gofrs/uuid"
)

func NewV4() uuid.UUID {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}
	return u
}
