package storage

import (
	"context"
	"time"

	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/errors"
)

type LikeStorage interface {
	RecordLike(ctx context.Context, actorRowID, objectRowID uuid.UUID, activity Payload) (uuid.UUID, error)
	RecordLikeAll(ctx context.Context, rowID uuid.UUID, createdAt, updatedAt time.Time, actorRowID, objectRowID uuid.UUID, activity Payload) (uuid.UUID, error)
	RemoveObjectLike(ctx context.Context, actorRowID, objectRowID uuid.UUID) error
}

func (s pgStorage) RecordLike(ctx context.Context, actorRowID, objectRowID uuid.UUID, activity Payload) (uuid.UUID, error) {
	rowID := NewV4()
	now := s.now()
	return s.RecordLikeAll(ctx, rowID, now, now, actorRowID, objectRowID, activity)
}

func (s pgStorage) RecordLikeAll(ctx context.Context, rowID uuid.UUID, createdAt, updatedAt time.Time, actorRowID, objectRowID uuid.UUID, activity Payload) (uuid.UUID, error) {
	query := `INSERT INTO object_likes (id, created_at, updated_at, actor_id, object_id, activity) VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT ON CONSTRAINT object_likes_uindex DO UPDATE SET updated_at = $3 RETURNING id`
	var id uuid.UUID
	err := s.db.QueryRowContext(ctx, query, rowID, createdAt, updatedAt, actorRowID, objectRowID, activity).Scan(&id)
	return id, errors.WrapObjectLikeUpsertFailedError(err)
}

func (s pgStorage) RemoveObjectLike(ctx context.Context, actorRowID, objectRowID uuid.UUID) error {
	query := `DELETE FROM object_likes WHERE actor_id = $1 AND object_id = $2`
	_, err := s.db.ExecContext(ctx, query, actorRowID, objectRowID)
	return errors.WrapObjectLikeDeleteFailedError(err)
}
