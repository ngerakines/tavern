package storage

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestActorRegex(t *testing.T) {
	ur := regexp.MustCompile(usernameRegex)
	mr := regexp.MustCompile(mentionRegex)
	tr := regexp.MustCompile(tagRegex)

	usernames := map[string]string{
		"nick":        "nick",
		"nick12345":   "nick12345",
		"!nick":       "nick",
		"nick!":       "nick",
		"nick123nick": "nick123nick",
	}

	mentions := map[string]string{
		"@nick@tavern.town":       "@nick@tavern.town",
		"@nick1@tavern.town":      "@nick1@tavern.town",
		"@nick!@tavern.town":      "",
		"@nick!tavern.town":       "",
		"@nick.tavern.town":       "",
		"pre\n@nick@tavern.town":  "@nick@tavern.town",
		"\n@nick@tavern.town":     "@nick@tavern.town",
		"@nick@tavern.town, post": "@nick@tavern.town",
	}

	tags := map[string][]string{
		"#what":        {"#what"},
		"#what #what":  {"#what", "#what"},
		"hello #world": {"#world"},
		"hello#world":  nil,
	}

	for i, e := range usernames {
		assert.Equalf(t, e, ur.FindString(i), "username '%s' matches '%s'", i, e)
	}

	for i, e := range mentions {
		assert.Equalf(t, e, mr.FindString(i), "mention '%s' matches '%s'", i, e)
	}
	for i, e := range tags {
		assert.Equalf(t, e, tr.FindAllString(i, -1), "tag '%s' matches '%s'", i, e)
	}
}
