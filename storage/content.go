package storage

import (
	"regexp"
)

var usernameRegex = `(\w+([\w\.-]+\w+)?)`
var mentionRegex = `(@\w+([\w_\.-]+\w+)?@[\w\.-]*[\w])`
var tagRegex = `(?:^|\B)([＃#]{1}(\w+))`

func FindMentionedActors(input string) []string {
	mr := regexp.MustCompile(mentionRegex)
	return mr.FindAllString(input, -1)
}

func FindMentionedTags(input string) []string {
	tr := regexp.MustCompile(tagRegex)
	return tr.FindAllString(input, -1)
}
