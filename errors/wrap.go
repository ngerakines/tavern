package errors

import (
	"errors"
)

var (
	New    = errors.New
	Is     = errors.Is
	As     = errors.As
	Unwrap = errors.Unwrap
)

type ErrorWrapper func(error) error
