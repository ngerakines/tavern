// +build ignore

package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("usage: storage_crud.go <start> [names]")
		fmt.Println("       <start> - integer, the starting counter position")
		fmt.Println("       [names] - space separated list of strings to create crud errors for")
		return
	}

	counter, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	w := csv.NewWriter(os.Stdout)

	for _, thing := range os.Args[2:] {
		templates := [][]string{
			[]string{"%sQueryFailed", "The %s query failed.", "QueryFailed"},
			[]string{"%sSelectFailed", "The select record operation for %s failed.", "SelectQueryFailed"},
			[]string{"%sInsertFailed", "The insert record operation for %s failed.", "InsertQueryFailed"},
			[]string{"%sUpsertFailed", "The upsert record operation for %s failed.", "QueryFailed"},
			[]string{"%sUpdateFailed", "The update record operation for %s failed.", "UpdateQueryFailed"},
			[]string{"%sDeleteFailed", "The delete record operation for %s failed.", "DeleteQueryFailed"},
			[]string{"Invalid%s", "The %s is invalid."},
			[]string{"%sNotFound", "The %s was not found.", "NotFound"},
		}

		for _, template := range templates {
			lower := strings.ToLower(strings.Join(Split(thing), " "))
			record := []string{
				fmt.Sprintf("%d", counter),
				"TAVDAT",
				fmt.Sprintf(template[0], thing),
				fmt.Sprintf(template[1], lower),
			}
			if len(template) == 3 {
				record = append(record, template[2])
			}
			if err := w.Write(record); err != nil {
				panic(err)
			}
			counter++
		}
	}

	w.Flush()

	if err := w.Error(); err != nil {
		panic(err)
	}
}

func Split(src string) (entries []string) {
	// don't split invalid utf8
	if !utf8.ValidString(src) {
		return []string{src}
	}
	entries = []string{}
	var runes [][]rune
	lastClass := 0
	class := 0
	// split into fields based on class of unicode character
	for _, r := range src {
		switch true {
		case unicode.IsLower(r):
			class = 1
		case unicode.IsUpper(r):
			class = 2
		case unicode.IsDigit(r):
			class = 3
		default:
			class = 4
		}
		if class == lastClass {
			runes[len(runes)-1] = append(runes[len(runes)-1], r)
		} else {
			runes = append(runes, []rune{r})
		}
		lastClass = class
	}
	// handle upper case -> lower case sequences, e.g.
	// "PDFL", "oader" -> "PDF", "Loader"
	for i := 0; i < len(runes)-1; i++ {
		if unicode.IsUpper(runes[i][0]) && unicode.IsLower(runes[i+1][0]) {
			runes[i+1] = append([]rune{runes[i][len(runes[i])-1]}, runes[i+1]...)
			runes[i] = runes[i][:len(runes[i])-1]
		}
	}
	// construct []string from results
	for _, s := range runes {
		if len(s) > 0 {
			entries = append(entries, string(s))
		}
	}
	return
}
