create table if not exists public.object_likes
(
    id         uuid                     not null
        constraint object_likes_pk primary key,
    created_at timestamp with time zone not null,
    updated_at timestamp with time zone not null,
    object_id  uuid                     not null,
    actor_id   uuid                     not null,
    activity   jsonb                    not null,
    constraint object_likes_uindex
        unique (actor_id, object_id)
);