package migrations

import (
	"fmt"
	"runtime"

	"github.com/getsentry/sentry-go"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	_ "github.com/lib/pq"
	"github.com/urfave/cli/v2"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/config"
	"github.com/ngerakines/tavern/g"
)

var Command = cli.Command{
	Name:  "migrate",
	Usage: "Run database migrations",
	Flags: []cli.Flag{
		&config.EnvironmentFlag,
		&config.DatabaseFlag,
	},
	Action: serverCommandAction,
}

func serverCommandAction(cliCtx *cli.Context) error {
	logger, err := config.Logger(cliCtx)
	if err != nil {
		return err
	}

	logger.Info("Starting",
		zap.String("command", cliCtx.Command.Name),
		zap.String("GOOS", runtime.GOOS),
		zap.String("env", cliCtx.String("environment")))

	sentryConfig, err := config.NewSentryConfig(cliCtx)
	if err != nil {
		return err
	}

	if sentryConfig.Enabled {
		err = sentry.Init(sentry.ClientOptions{
			Dsn:         sentryConfig.Key,
			Environment: cliCtx.String("environment"),
			Release:     fmt.Sprintf("%s-%s", g.Release, g.GitCommit),
		})
		if err != nil {
			return err
		}
		sentry.ConfigureScope(func(scope *sentry.Scope) {
			scope.SetTags(map[string]string{"container": "migrate"})
		})
		defer sentry.Recover()
	}

	migrationSources := bindata.Resource(AssetNames(),
		func(name string) ([]byte, error) {
			return Asset(name)
		})
	migrationData, err := bindata.WithInstance(migrationSources)
	if err != nil {
		return err
	}

	db, dbClose, err := config.DB(cliCtx, logger)
	if err != nil {
		return err
	}
	defer dbClose()

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("go-bindata", migrationData, "postgres", driver)
	if err != nil {
		return err
	}

	return m.Up()
}
