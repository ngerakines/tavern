create table if not exists public.groups
(
    id                  uuid                     not null
        constraint groups_pk primary key,
    created_at          timestamp with time zone not null,
    updated_at          timestamp with time zone not null,
    name                varchar                  not null,
    public_key          text                     not null,
    private_key         text                     not null,
    display_name        varchar                  not null,
    about               text                     not null,
    accept_followers    boolean default true     not null,
    default_member_role integer default 0        not null,
    allow_remote        boolean default true     not null,
    owner               uuid                     not null,
    actor_id            uuid                     not null,
    constraint groups_name_uindex
        unique (name)
);

create table if not exists public.group_members
(
    id                  uuid                     not null
        constraint group_followers_pk
            primary key,
    created_at          timestamp with time zone not null,
    updated_at          timestamp with time zone not null,
    group_actor_id      uuid                     not null,
    member_actor_id     uuid                     not null,
    activity            jsonb                    not null,
    relationship_status integer default 0        not null,
    member_role         integer default 0        not null,
    constraint group_members_uindex
        unique (group_actor_id, member_actor_id)
);

create table if not exists public.group_invitations
(
    id              uuid                     not null
        constraint group_invitations_pk
            primary key,
    created_at      timestamp with time zone not null,
    updated_at      timestamp with time zone not null,
    group_actor_id  uuid                     not null,
    member_actor_id uuid                     not null,
    activity        jsonb                    not null,
    role            integer default 0        not null,
    constraint group_invitations_uindex
        unique (group_actor_id, member_actor_id)
);

create table if not exists public.group_boosts
(
    id             uuid                     not null
        constraint group_boosts_pk
            primary key,
    created_at     timestamp with time zone not null,
    updated_at     timestamp with time zone not null,
    group_actor_id uuid                     not null,
    object_id      uuid                     not null,
    constraint group_boosts_uindex
        unique (group_actor_id, object_id)
);
