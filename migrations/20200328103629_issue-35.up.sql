create table if not exists public.object_subscriptions
(
    id         uuid                     not null
        constraint object_subscriptions_pk primary key,
    created_at timestamp with time zone not null,
    updated_at timestamp with time zone not null,
    object_id  uuid                     not null,
    actor_id   uuid                     not null,
    constraint object_subscriptions_uindex
        unique (object_id, actor_id)
);