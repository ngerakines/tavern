package config

import (
	"github.com/urfave/cli/v2"
)

var EnvironmentFlag = cli.StringFlag{Name: "environment", Usage: "Set the environment the application is running in.", EnvVars: []string{"ENVIRONMENT"}, Value: "development"}

var ListenFlag = cli.StringFlag{Name: "listen", Usage: "Configure the server to listen to this interface.", EnvVars: []string{"LISTEN"}, Value: "0.0.0.0:8000"}

var DomainFlag = cli.StringFlag{Name: "domain", Usage: "Set the website domain.", Value: "tavern.ngrok.io", EnvVars: []string{"DOMAIN"}}
