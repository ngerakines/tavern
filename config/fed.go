package config

import (
	"github.com/urfave/cli/v2"
)

type FedConfig struct {
	AllowFollowObject        bool
	AllowAutoAcceptFollowers bool
	AllowInboxForwarding     bool
}

var AllowFollowObjectFlag = cli.BoolFlag{
	Name:    "allow-follow-object",
	Usage:   "Allow actors to follow objects",
	EnvVars: []string{"ALLOW_OBJECT_FOLLOW"},
	Value:   false,
}

var AllowAutoAcceptFollowersFlag = cli.BoolFlag{
	Name:    "allow-auto-accept-followers",
	Usage:   "Allow users to turn on automatically accepting follow requests.",
	EnvVars: []string{"ALLOW_AUTO_ACCEPT_FOLLOWERS"},
	Value:   true,
}

var AllowInboxForwardingFlag = cli.BoolFlag{
	Name:    "allow-inbox-forwarding",
	Usage:   "Allow messages sent to your inbox to be forwarded to other inboxes.",
	EnvVars: []string{"ALLOW_INBOX_FORWARDING"},
	Value:   false,
}

func NewFedConfig(cliCtx *cli.Context) (FedConfig, error) {
	cfg := FedConfig{
		AllowAutoAcceptFollowers: cliCtx.Bool("allow-auto-accept-followers"),

		AllowFollowObject:    cliCtx.Bool("allow-reply-collection-updates"),
		AllowInboxForwarding: cliCtx.Bool("allow-inbox-forwarding"),
	}
	return cfg, nil
}
