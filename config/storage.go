package config

import (
	"database/sql"

	"github.com/urfave/cli/v2"
	"go.uber.org/zap"
)

var DatabaseFlag = cli.StringFlag{
	Name:    "database",
	Usage:   "Select the database to connect to.",
	Value:   "host=localhost port=5432 user=postgres dbname=tavern password=password sslmode=disable",
	EnvVars: []string{"DATABASE"},
}

var MigrationsPathFlag = cli.StringFlag{
	Name:    "migrations",
	Usage:   "The path to database migrations",
	Value:   "file://migrations/",
	EnvVars: []string{"DATABASE_MIGRATIONS"},
}

func DB(c *cli.Context, logger *zap.Logger) (*sql.DB, func(), error) {
	dbConnInfo := c.String("database")
	db, err := sql.Open("postgres", dbConnInfo)
	if err != nil {
		return nil, nil, err
	}
	dbClose := func() {
		if closeErr := db.Close(); closeErr != nil {
			logger.Error("error closing database connection", zap.Error(closeErr))
		}
	}
	if err := db.Ping(); err != nil {
		dbClose()
		return nil, nil, err
	}
	return db, dbClose, nil
}
