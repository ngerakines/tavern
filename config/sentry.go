package config

import (
	"fmt"

	"github.com/urfave/cli/v2"
)

type SentryConfig struct {
	Enabled bool
	Key     string
}

var EnableSentryFlag = cli.BoolFlag{
	Name:    "enable-sentry",
	Usage:   "Enable sentry integration",
	EnvVars: []string{"ENABLE_SENTRY"},
	Value:   false,
}

var SentryFlag = cli.StringFlag{
	Name:    "sentry",
	Usage:   "Configure the sentry key to use.",
	EnvVars: []string{"SENTRY_KEY"},
	Value:   "",
}

func NewSentryConfig(cliCtx *cli.Context) (SentryConfig, error) {
	cfg := SentryConfig{
		Enabled: cliCtx.Bool("enable-sentry"),
		Key:     cliCtx.String("sentry"),
	}
	if cfg.Enabled && len(cfg.Key) == 0 {
		return cfg, fmt.Errorf("invalid sentry config: sentry enabled with empty key")
	}
	return cfg, nil
}
