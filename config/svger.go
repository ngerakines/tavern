package config

import (
	"github.com/urfave/cli/v2"
)

var EnableSVGerFlag = cli.BoolFlag{
	Name:    "enable-svger",
	Usage:   "Enable svger integration",
	EnvVars: []string{"ENABLE_SVGER"},
	Value:   false,
}

var SVGerEndpointFlag = cli.StringFlag{
	Name:    "svger",
	Usage:   "The SVGer to interact with.",
	EnvVars: []string{"SVGER"},
	Value:   "http://localhost:5003/",
}

type SVGerConfig struct {
	Enabled  bool
	Location string
}

func NewSVGerConfig(cliCtx *cli.Context) SVGerConfig {
	return SVGerConfig{
		Enabled:  cliCtx.Bool("enable-svger"),
		Location: cliCtx.String("svger"),
	}
}
