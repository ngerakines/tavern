package config

import (
	"bytes"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/urfave/cli/v2"

	"github.com/ngerakines/tavern/translations"
)

var TranslationsFlag = cli.StringFlag{
	Name:    "translations",
	Usage:   "The path translations are located",
	EnvVars: []string{"TRANSLATIONS"},
	Value:   "translations",
}

func Trans(cliCtx *cli.Context) (*ut.UniversalTranslator, error) {
	english := en.New()
	utrans := ut.New(english, english)

	err := loadTranslations(utrans)
	if err != nil {
		return nil, err
	}

	err = utrans.VerifyTranslations()
	if err != nil {
		return nil, err
	}

	return utrans, nil
}

func loadTranslations(utrans *ut.UniversalTranslator) error {
	for _, source := range translations.AssetNames() {
		data, err := translations.Asset(source)
		if err != nil {
			return err
		}
		if err = utrans.ImportByReader(ut.FormatJSON, bytes.NewReader(data)); err != nil {
			return err
		}
	}
	return nil
}
