package config

import (
	"github.com/urfave/cli/v2"
)

var EnablePublisherFlag = cli.BoolFlag{
	Name:    "enable-publisher",
	Usage:   "Enable publisher integration",
	EnvVars: []string{"ENABLE_PUBLISHER"},
	Value:   false,
}

var PublisherLocationFlag = cli.StringFlag{
	Name:    "publisher-location",
	Usage:   "The publisher to interact with.",
	EnvVars: []string{"PUBLISHER"},
	Value:   "http://localhost:9200/",
}

var PublisherCallbackLocationFlag = cli.StringFlag{
	Name:    "publisher-callback",
	Usage:   "The publisher to interact with.",
	EnvVars: []string{"PUBLISHER_CALLBACK"},
	Value:   "http://localhost:8000/webhooks/publisher",
}

type PublisherConfig struct {
	Enabled  bool
	Location string
}

func NewPublisherConfig(cliCtx *cli.Context) PublisherConfig {
	return PublisherConfig{
		Enabled:  cliCtx.Bool("enable-publisher"),
		Location: cliCtx.String("publisher-location"),
	}
}
