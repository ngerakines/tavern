package config

import (
	"context"
	"io"

	"github.com/urfave/cli/v2"

	"github.com/ngerakines/tavern/common"
)

var AssetStorageFlag = cli.StringFlag{
	Name:    "asset-storage",
	Usage:   "The type of asset storage system to use",
	Value:   "file",
	EnvVars: []string{"ASSET_STORAGE"},
}

var AssetStorageFileBaseFlag = cli.StringFlag{
	Name:    "asset-storage-file-base",
	Usage:   "The base path used for file asset storage.",
	Value:   "./assets/",
	EnvVars: []string{"ASSET_STORAGE_FILE_BASE"},
}

var AssetStorageRemoteAllowFlag = cli.StringSliceFlag{
	Name:    "asset-storage-remote-allow",
	Usage:   "Asset storage remote allow list.",
	EnvVars: []string{"ASSET_STORAGE_REMOTE_ALLOW"},
}

var AssetStorageRemoteDenyFlag = cli.StringSliceFlag{
	Name:    "asset-storage-remote-deny",
	Usage:   "Asset storage remote deny list.",
	EnvVars: []string{"ASSET_STORAGE_REMOTE_DENY"},
}

var AssetStorageRemoteMaxFlag = cli.Int64Flag{
	Name:    "asset-storage-max",
	Usage:   "Asset storage remote max file size.",
	Value:   5000000,
	EnvVars: []string{"ASSET_STORAGE_REMOTE_MAX"},
}

type AssetStore interface {
	Close() error
	Create(ctx context.Context, location string, reader io.Reader) (string, error)
	Delete(ctx context.Context, location string) error
	Exists(ctx context.Context, location string) (bool, error)
	Read(ctx context.Context, location string) (io.ReadCloser, error)
	Upload(ctx context.Context, checksum string, source string) (string, error)
}

type AssetStorageConfig struct {
	Type         string
	FileBasePath string
	MaxFileSize  int64

	AllowDomains []common.AllowDenyMatcher
	DenyDomains  []common.AllowDenyMatcher
}

func AssetStorage(c *cli.Context) AssetStorageConfig {
	allowDomains := make([]common.AllowDenyMatcher, 0)
	denyDomains := make([]common.AllowDenyMatcher, 0)

	denyDomains = append(denyDomains, common.NewAllowDenyMatcher(c.String("domain")))

	for _, value := range c.StringSlice("asset-storage-remote-allow") {
		allowDomains = append(allowDomains, common.NewAllowDenyMatcher(value))
	}

	for _, value := range c.StringSlice("asset-storage-remote-deny") {
		denyDomains = append(denyDomains, common.NewAllowDenyMatcher(value))
	}

	return AssetStorageConfig{
		Type:         c.String("asset-storage"),
		FileBasePath: c.String("asset-storage-file-base"),
		MaxFileSize:  c.Int64("asset-storage-max"),

		AllowDomains: allowDomains,
		DenyDomains:  denyDomains,
	}
}
