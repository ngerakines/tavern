package config

import (
	"github.com/urfave/cli/v2"
)

type GroupConfig struct {
	EnableGroups                  bool
	AllowAutoAcceptGroupFollowers bool
	AllowRemoteGroupFollowers     bool
	DefaultGroupMemberRole        int
	DefaultDirectoryOptIn         bool
}

var EnableGroupsFlag = cli.BoolFlag{
	Name:    "enable-groups",
	Usage:   "Enable groups",
	EnvVars: []string{"ENABLE_GROUPS"},
	Value:   true,
}

var AllowAutoAcceptGroupFollowersFlag = cli.BoolFlag{
	Name:    "allow-auto-accept-group-followers",
	Usage:   "Allow groups to turn on automatically accepting follow requests.",
	EnvVars: []string{"ALLOW_AUTO_ACCEPT_GROUP_FOLLOWERS"},
	Value:   true,
}

var AllowRemoteFollowersFlag = cli.BoolFlag{
	Name:    "allow-remote-group-followers",
	Usage:   "Allow non-local users to follow groups.",
	EnvVars: []string{"ALLOW_REMOTE_GROUP_FOLLOWERS"},
	Value:   true,
}

var DefaultGroupMemberRoleFlag = cli.IntFlag{
	Name:    "default-group-member-role",
	Usage:   "The default member role for groups.",
	EnvVars: []string{"DEFAULT_GROUP_MEMBER_ROLE"},
	Value:   1,
}

var DefaultDirectoryOptInFlag = cli.BoolFlag{
	Name:    "default-directory-opt-in",
	Usage:   "The default directory opt-in value for groups.",
	EnvVars: []string{"DEFAULT_GROUP_DIRECTORY_OPT_IN"},
	Value:   true,
}

func NewGroupConfig(cliCtx *cli.Context) (GroupConfig, error) {
	cfg := GroupConfig{
		EnableGroups:                  cliCtx.Bool("enable-groups"),
		AllowAutoAcceptGroupFollowers: cliCtx.Bool("allow-auto-accept-group-followers"),
		AllowRemoteGroupFollowers:     cliCtx.Bool("allow-remote-group-followers"),
		DefaultGroupMemberRole:        cliCtx.Int("default-group-member-role"),
		DefaultDirectoryOptIn:         cliCtx.Bool("default-directory-opt-in"),
	}
	return cfg, nil
}
