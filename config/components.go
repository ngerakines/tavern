package config

import (
	"encoding/hex"
	"strings"

	"github.com/gofrs/uuid"
	"github.com/urfave/cli/v2"
	"go.uber.org/zap"
)

var ProductionEnvironment = "production"

var SecretFlag = cli.StringFlag{
	Name:    "secret",
	Usage:   "Set the server secret",
	EnvVars: []string{"SECRET"},
}

func Logger(c *cli.Context) (*zap.Logger, error) {
	if c.String("environment") == ProductionEnvironment {
		return zap.NewProduction()
	}
	return zap.NewDevelopment()
}

func ListenAddress(cliCtx *cli.Context) string {
	if listen := cliCtx.String("listen"); len(listen) > 0 {
		return listen
	}
	return ":8080"
}

func Secret(cliCtx *cli.Context) ([]byte, error) {
	secret := cliCtx.String("secret")
	secret = strings.TrimSpace(secret)
	if len(secret) > 0 {
		decoded, err := hex.DecodeString(secret)
		if err != nil {
			return nil, err
		}
		return first32Bytes(decoded), nil
	}
	id, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	return first32Bytes(id.Bytes()), nil
}

func first32Bytes(in []byte) []byte {
	if len(in) > 32 {
		return in[0:32]
	}
	return in
}
