package g

import (
	"fmt"
)

var Release string
var GitCommit string
var BuildTime string

func Version() string {
	return Release
}

func UserAgent() string {
	return fmt.Sprintf("tavern/%s (+https://gitlab.com/ngerakines/tavern)", Version())
}
