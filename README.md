# tavern

A minimalistic Activity Pub server. Think [mastodon](https://joinmastodon.org/), but smaller and with fewer features.

**_This project is in active development and should not be used in production._**

Roadmap: https://gitlab.com/ngerakines/tavern/-/wikis/Roadmap

Milestones: https://gitlab.com/ngerakines/tavern/-/milestones

# Quick Start

The quickest way to get up and running is with docker-compose.

If you are building from source, be sure to run `docker-compose build`. These instructions assume all of the required configuration is set.

1. docker-compose up -d db svger
2. docker-compose run web migrate
3. docker-compose run web init-service --pem /service.pem
4. docker-compose run web init-admin --admin-email=nick.gerakines@gmail.com --admin-password=password --admin-name=nick
5. docker-compose up -d

# Contributing

First, please read the [CONTRIBUTING](CONTRIBUTING.md) document. This project adheres to the contributors covenant, https://www.contributor-covenant.org/.

The build and release process involves code generation in a few ways that aren't obvious.

1. `go generate ./...` generates error structures and sources used to bundle files in the compiled binary.
2. `openssl dgst -sha384 -binary the_file.whatever | openssl base64 -A` is used to create SRI hashes (`integrity="sha384-decafbad"`) in included CSS and JS files.

The `-tags prod` argument can be used to indicate prod vs non-prod sources. When the `prod` flag **is not** present, asset packages will load content from disk, but will still contain file path references. In other words, whenever you create or delete files including templates, migrations, or translations, you must run `go generate ./...`.

## Docker Builds

    $ export TAVERN_RELEASE_CODE=$(git describe --always)
    $ export TAVERN_GIT_COMMIT=$(git log --format="%H" -n 1)
    $ export TAVERN_BUILD_TIME=$(date --rfc-3339=seconds)
    $ docker build --build-arg=GIT_COMMIT=$TAVERN_GIT_COMMIT --build-arg=RELEASE_CODE=$TAVERN_RELEASE_CODE --build-arg=BUILD_TIME="$TAVERN_BUILD_TIME" -t ngerakines/tavern:latest  --target=tavern .

# License

MIT License

Copyright (c) 2020 Nick Gerakines
