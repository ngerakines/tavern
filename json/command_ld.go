package json

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/kr/pretty"
	"github.com/piprate/json-gold/ld"
	"github.com/urfave/cli/v2"

	"github.com/ngerakines/tavern/storage"
)

/*
{"@context":["https://www.w3.org/ns/activitystreams",{"ostatus":"http://ostatus.org#","atomUri":"ostatus:atomUri","inReplyToAtomUri":"ostatus:inReplyToAtomUri","conversation":"ostatus:conversation","sensitive":"as:sensitive","toot":"http://joinmastodon.org/ns#","votersCount":"toot:votersCount"}],"id":"https://mastodon.social/users/ngerakines/statuses/103849134994060072/activity","type":"Create","actor":"https://mastodon.social/users/ngerakines","published":"2020-03-19T10:00:45Z","to":["https://www.w3.org/ns/activitystreams#Public"],"cc":["https://mastodon.social/users/ngerakines/followers"],"object":{"id":"https://mastodon.social/users/ngerakines/statuses/103849134994060072","type":"Note","summary":null,"inReplyTo":null,"published":"2020-03-19T10:00:45Z","url":"https://mastodon.social/@ngerakines/103849134994060072","attributedTo":"https://mastodon.social/users/ngerakines","to":["https://www.w3.org/ns/activitystreams#Public"],"cc":["https://mastodon.social/users/ngerakines/followers"],"sensitive":false,"atomUri":"https://mastodon.social/users/ngerakines/statuses/103849134994060072","inReplyToAtomUri":null,"conversation":"tag:mastodon.social,2020-03-19:objectId=160727436:objectType=Conversation","content":"\u003cp\u003eld+json is kind of weird\u003c/p\u003e","contentMap":{"lb":"\u003cp\u003eld+json is kind of weird\u003c/p\u003e"},"attachment":[],"tag":[],"replies":{"id":"https://mastodon.social/users/ngerakines/statuses/103849134994060072/replies","type":"Collection","first":{"type":"CollectionPage","next":"https://mastodon.social/users/ngerakines/statuses/103849134994060072/replies?only_other_accounts=true\u0026page=true","partOf":"https://mastodon.social/users/ngerakines/statuses/103849134994060072/replies","items":[]}}},"signature":{"type":"RsaSignature2017","creator":"https://mastodon.social/users/ngerakines#main-key","created":"2020-03-19T10:00:45Z","signatureValue":"OA35CkmjxaUGh/aO6HYbqQ9T2Rkj6Agt0vp+Qif7/PUBuMpRed/eXg4O+NXkIEvv6s4DHhwsQbUuQf792fj4u1KlUTQVGVrMzx1uBvf06ye/Gz74jpn9I2h41a3B3MZP02tgBXWrB4PaNBEFlg58y8vk3yK/FTRZ4hlcy5FFFZKJNzfP8F2UqQb61aqwkavEwme2Tn8KpDWR/HoR8Qh2WsymESObX/4GxavuZD9hxszaqhOterpZuDOT0FbMS32NsLXUr7Vhlr3DYkau+54icUbl9qlD1JCl/0VN7fQ6Xcu8LX4nQF74BwJtQrB7h3bIfCFVP7uRBhLmUkAahLkotQ=="}}
*/

var DebugJSONLDCommand = cli.Command{
	Name:   "debug-jsonld",
	Flags:  []cli.Flag{},
	Action: DebugJSONLDAction,
}

func DebugJSONLDAction(cliCtx *cli.Context) error {
	reader := bufio.NewReader(os.Stdin)
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		pretty.Println(err)
		return err
	}
	fmt.Println("=== RAW ===")
	fmt.Println(string(data))

	doc, err := storage.PayloadFromBytes(data)
	if err != nil {
		pretty.Println(err)
		return err
	}
	fmt.Println("=== PRETTY ===")
	pretty.Println(doc)

	// activityContext := []interface{}{
	// 	// "https://www.w3.org/ns/activitystreams",
	// 	// "https://aqueous-sea-10253.herokuapp.com/schemas/litepub-0.1.jsonld",
	// }
	// activityContext, _ := storage.JSONMap(doc, "@context")

	documentContext := map[string]interface{}{
		"@context": "https://www.w3.org/ns/activitystreams",
	}

	fmt.Println("=== JSON LD ===")
	result, err := compactJSONLD(doc, documentContext)
	if err != nil {
		pretty.Println(err)
		return err
	}
	pretty.Println(result)

	triples, err := rdf(result)
	if err != nil {
		pretty.Println(err)
		return err
	}
	pretty.Println(triples)

	return nil
}

func compactJSONLD(document, context map[string]interface{}) (map[string]interface{}, error) {
	proc := ld.NewJsonLdProcessor()
	options := ld.NewJsonLdOptions("")
	// options.ProcessingMode = ld.JsonLd_1_1
	// options.DocumentLoader = DebugDocLoader{ld.NewDefaultDocumentLoader(http.DefaultClient)}
	return proc.Compact(document, context, options)
}

func rdf(document map[string]interface{}) (interface{}, error) {
	proc := ld.NewJsonLdProcessor()
	options := ld.NewJsonLdOptions("")
	options.Format = "application/n-quads"
	return proc.ToRDF(document, options)
}

type DebugDocLoader struct {
	DocumentLoader ld.DocumentLoader
}

func (ddl DebugDocLoader) LoadDocument(u string) (*ld.RemoteDocument, error) {
	fmt.Println("=== LoadDocument ===")
	pretty.Println(u)
	r, err := ddl.DocumentLoader.LoadDocument(u)
	pretty.Println(r)
	pretty.Println(err)
	return r, err
}
