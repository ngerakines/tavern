package publisher

import (
	"context"
	"time"

	"github.com/oklog/run"
	"go.uber.org/zap"
)

type Worker interface {
	Run() error
	Shutdown(context.Context) error
}

func RunWorker(group *run.Group, logger *zap.Logger, w Worker) {
	group.Add(func() error {
		logger.Info("starting publisher")
		return ignoreCanceled(w.Run())
	}, func(error) {
		shutdownCtx, shutdownCtxCancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer shutdownCtxCancel()
		logger.Info("stopping publisher")
		err := ignoreCanceled(w.Shutdown(shutdownCtx))
		if err != nil {
			logger.Error("error stopping publisher", zap.Error(err))
		}
	})
}

func ignoreCanceled(err error) error {
	if err == nil || err == context.Canceled {
		return nil
	}
	return err
}
