package publisher

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type handlers struct {
	q *queue
}

type submission struct {
	Key         string `form:"key" binding:"required"`
	KeyID       string `form:"key_id" binding:"required"`
	Destination string `form:"destination" binding:"required"`
	ActivityID  string `form:"activity_id" binding:"required"`
	Payload     string `form:"payload" binding:"required"`
}

func (h handlers) handleSubmission(c *gin.Context) {
	var s submission
	if err := c.ShouldBind(&s); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return
	}
	err := h.q.Add(s.Destination, s.KeyID, s.Key, s.ActivityID, s.Payload)
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusAccepted)
}
