package publisher

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/yukimochi/httpsig"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/common"
)

type publisher struct {
	logger   *zap.Logger
	q        *queue
	callback callback

	ctx        context.Context
	cancel     context.CancelFunc
	httpClient common.HTTPClient
}

func (r *publisher) Run() error {
	r.ctx, r.cancel = context.WithCancel(context.Background())
	defer r.cancel()

	for {
		select {
		case <-time.After(time.Second):
			if err := r.work(); err != nil {
				r.logger.Error("error processing work", zap.Error(err))
			}
		case <-r.ctx.Done():
			return ignoreCanceled(r.ctx.Err())
		}
	}
}

func (r *publisher) Shutdown(parent context.Context) error {
	r.cancel()
	select {
	case <-parent.Done():
		return parent.Err()
	case <-r.ctx.Done():
		return r.ctx.Err()
	}
}

func (r *publisher) work() error {
	jobs := r.q.Take()
	if len(jobs) == 0 {
		return nil
	}
	destination := jobs[0].Destination
	activityID := jobs[0].ActivityID

	sigConfig := []httpsig.Algorithm{httpsig.RSA_SHA256}
	headersToSign := []string{httpsig.RequestTarget, "date" ,"digest", "content-type"}
	signer, algo, err := httpsig.NewSigner(sigConfig, headersToSign, httpsig.Signature)
	if err != nil {
		return r.callback(activityID, destination, err)
	}

	dateHeader := time.Now().UTC().Format(http.TimeFormat)
	for i, job := range jobs {
		if i > 0 {
			time.Sleep(1 * time.Second)
		}

		checksum := sha256.Sum256([]byte(job.Payload))
		digest := base64.StdEncoding.EncodeToString(checksum[:])

		request, err := http.NewRequest("POST", job.Destination, strings.NewReader(job.Payload))
		if err != nil {
			return r.callback(activityID, destination, err)
		}
		request.Header.Add("content-type", "application/json")
		request.Header.Add("date", dateHeader)
		request.Header.Add("digest", fmt.Sprintf("SHA-256=%s", digest))
		request.Header.Add("X-ALG", string(algo))
		if err = signer.SignRequest(job.PrivateKey, job.KeyID, request); err != nil {
			return r.callback(activityID, destination, err)
		}
		resp, err := r.httpClient.Do(request)
		if err != nil {
			return r.callback(activityID, destination, err)
		}

		defer resp.Body.Close()

		if resp.StatusCode >= 200 && resp.StatusCode < 300 {
			continue
		}

		lr := io.LimitReader(resp.Body, 500000)

		data, err := ioutil.ReadAll(lr)
		if err != nil {
			return r.callback(activityID, destination, err)
		}

		return r.callback(activityID, destination, fmt.Errorf("unexpected response: %d %s", resp.StatusCode, string(data)))
	}

	return r.callback(activityID, destination, nil)
}
