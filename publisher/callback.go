package publisher

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/ngerakines/tavern/common"
)

type tavernCallback struct {
	location   string
	httpClient common.HTTPClient
}

type callback func(string, string, error) error

func (cb tavernCallback) callback(activityID, destination string, err error) error {
	obj := map[string]string{
		"destination": destination,
		"activity":    activityID,
		"status":      "ok",
	}
	if err != nil {
		obj["status"] = "error"
		obj["error"] = err.Error()
	}

	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", cb.location, bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Add("content-type", "application/json")
	req.Header.Add("date", time.Now().UTC().Format(http.TimeFormat))

	resp, err := cb.httpClient.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 500000))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response: %d %s", resp.StatusCode, string(body))
	}

	return nil
}
