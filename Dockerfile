FROM golang:1.13-alpine3.11 as tavern-build
LABEL maintainer="Nick Gerakines <nick.gerakines@gmail.com>"
WORKDIR /src
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go generate -tags prod ./...
ARG GIT_COMMIT
ARG RELEASE_CODE
ARG BUILD_TIME
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go install -ldflags "-w -s -extldflags '-static' -X main.GitCommit=$GIT_COMMIT -X main.ReleaseCode=$RELEASE_CODE -X 'main.BuildTime=$BUILD_TIME'" github.com/ngerakines/tavern/...

FROM alpine:3.11 as tavern
RUN apk add --no-cache --update ca-certificates tzdata
RUN mkdir -p /app
WORKDIR /app
COPY --from=tavern-build /src/public /app/public
COPY --from=tavern-build /go/bin/tavern /go/bin/
EXPOSE 5000
# HEALTHCHECK --interval=5m --timeout=3s CMD curl -f http://localhost:5000/ || exit 1
ENTRYPOINT ["/go/bin/tavern"]
CMD ["server"]
