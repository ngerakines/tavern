module github.com/ngerakines/tavern

go 1.13

require (
	github.com/buckket/go-blurhash v1.0.3
	github.com/foolin/goview v0.2.0
	github.com/getsentry/sentry-go v0.4.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-contrib/zap v0.0.0-20191128031730-d12829f8f61b
	github.com/gin-gonic/gin v1.5.0
	github.com/go-bindata/go-bindata v3.1.2+incompatible
	github.com/go-playground/locales v0.12.1
	github.com/go-playground/universal-translator v0.16.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang-migrate/migrate/v4 v4.9.1
	github.com/kr/pretty v0.1.0
	github.com/lib/pq v1.3.0
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/oklog/run v1.0.0
	github.com/piprate/json-gold v0.3.0
	github.com/prometheus/client_golang v1.5.1
	github.com/russross/blackfriday/v2 v2.0.1
	github.com/sslhound/herr v1.4.1 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/teacat/noire v1.0.0
	github.com/urfave/cli/v2 v2.2.0
	github.com/yukimochi/httpsig v0.1.3
	go.uber.org/zap v1.13.0
	golang.org/x/crypto v0.0.0-20200221170553-0f24fbd83dfb
	golang.org/x/tools v0.0.0-20200228224639-71482053b885 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
