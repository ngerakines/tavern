package job

import (
	"context"
	"net/http"
	"time"

	"go.uber.org/zap"

	"github.com/ngerakines/tavern/asset"
	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/config"
	"github.com/ngerakines/tavern/storage"
)

type assetDownload struct {
	ctx                context.Context
	cancel             context.CancelFunc
	logger             *zap.Logger
	queue              common.StringQueue
	storage            storage.Storage
	assetStorage       asset.Storage
	httpClient         common.HTTPClient
	assetStorageConfig config.AssetStorageConfig
	fedConfig          config.FedConfig
}

func NewAssetDownloadJob(logger *zap.Logger, queue common.StringQueue, storage storage.Storage, assetStorage asset.Storage, config config.AssetStorageConfig, fedConfig config.FedConfig, httpClient *http.Client) Job {
	return &assetDownload{
		logger:             logger,
		queue:              queue,
		storage:            storage,
		assetStorage:       assetStorage,
		httpClient:         httpClient,
		assetStorageConfig: config,
		fedConfig:          fedConfig,
	}
}

func (job *assetDownload) Run(parent context.Context) error {
	job.ctx, job.cancel = context.WithCancel(parent)
	defer job.cancel()

	for {
		select {
		case <-time.After(time.Second):
			err := job.work()
			if err != nil {
				job.logger.Error("error processing work", zap.Error(err))
				return err
			}
		case <-job.ctx.Done():
			return ignoreCanceled(job.ctx.Err())
		}
	}
}

func (job *assetDownload) Shutdown(parent context.Context) error {
	job.cancel()
	select {
	case <-parent.Done():
		return parent.Err()
	case <-job.ctx.Done():
		return job.ctx.Err()
	}
}

func (job *assetDownload) work() error {
	work, err := job.queue.Take()
	if err != nil {
		return err
	}
	if len(work) == 0 {
		return nil
	}

	a := &asset.Agent{
		AssetStorage:       job.assetStorage,
		DataStorage:        job.storage,
		HTTPClient:         job.httpClient,
		AssetStorageConfig: job.assetStorageConfig,
		FedConfig:          job.fedConfig,
	}
	_, err = a.HandleImage(job.ctx, work)
	if err != nil {
		job.logger.Warn("error getting asset", zap.Error(err), zap.String("location", work))
	}
	return nil
}
