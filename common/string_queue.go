package common

import (
	"sync"
)

type stringQueue struct {
	values []string
	lock   sync.Mutex
}

type StringQueue interface {
	Add(value string) error
	Take() (string, error)
}

func NewStringQueue() StringQueue {
	return &stringQueue{
		values: make([]string, 0),
	}
}

func (q *stringQueue) Add(value string) error {
	q.lock.Lock()
	defer q.lock.Unlock()

	q.values = append(q.values, value)

	return nil
}

func (q *stringQueue) Take() (string, error) {
	q.lock.Lock()
	defer q.lock.Unlock()

	if len(q.values) == 0 {
		return "", nil
	}

	var value string
	value, q.values = q.values[0], q.values[1:]

	return value, nil
}
