package common

import (
	"github.com/gofrs/uuid"
)

type StringsMultiMap map[string]*UniqueStrings

func (mm StringsMultiMap) Add(key string, values ...string) {
	existingValues, ok := mm[key]
	if !ok {
		existingValues = NewUniqueStrings()
	}
	existingValues.Add(values...)
	mm[key] = existingValues
}


type UUIDsMultiMap map[uuid.UUID]*UniqueUUIDs

func (mm UUIDsMultiMap) Add(key uuid.UUID, values ...uuid.UUID) {
	existingValues, ok := mm[key]
	if !ok {
		existingValues = NewUniqueUUIDs()
	}
	existingValues.Add(values...)
	mm[key] = existingValues
}

func (mm UUIDsMultiMap) Flatten() map[uuid.UUID][]uuid.UUID {
	results := make(map[uuid.UUID][]uuid.UUID)
	for key, values := range mm {
		results[key] = values.Values
	}
	return results
}

