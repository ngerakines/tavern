package common

type SeenStringQueue struct {
	Seen  map[string]bool
	Queue []string
	Taken []string
}

func (q *SeenStringQueue) Add(value string) bool {
	if _, seen := q.Seen[value]; seen {
		return false
	}
	q.Seen[value] = true
	q.Queue = append(q.Queue, value)
	return true
}

func (q *SeenStringQueue) Take() (string, bool) {
	if len(q.Queue) == 0 {
		return "", false
	}
	var next string
	next, q.Queue = q.Queue[0], q.Queue[1:]
	q.Taken = append(q.Taken, next)
	return next, true
}

func (q *SeenStringQueue) Empty() bool {
	return len(q.Queue) == 0
}
