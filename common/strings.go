package common

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/gofrs/uuid"
)

type FilterFunc func(string) bool

type UniqueStrings struct {
	Tracking map[string]struct{}
	Values   []string
}

type UniqueUUIDs struct {
	Tracking map[uuid.UUID]struct{}
	Values   []uuid.UUID
}

func IntRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}

func StringIntRange(min, max int) []string {
	results := make([]string, max-min+1)
	for i, v := range IntRange(min, max) {
		results[i] = strconv.Itoa(v)
	}
	return results
}

func MapStrings(in []string, transform func(string) string) []string {
	results := make([]string, len(in))
	for i, v := range in {
		results[i] = transform(v)
	}
	return results
}

func AddPrefix(prefix string) func(string) string {
	return func(in string) string {
		return fmt.Sprintf("%s%s", prefix, in)
	}
}

func Dollar(in string) string {
	return fmt.Sprintf("$%s", in)
}

func DollarForEach(max int) []string {
	return MapStrings(StringIntRange(1, max), Dollar)
}

func DollarForEachFrom(min, max int) []string {
	return MapStrings(StringIntRange(min, max), Dollar)
}

func StringsToInterfaces(input []string) []interface{} {
	results := make([]interface{}, len(input))
	for i, value := range input {
		results[i] = value
	}
	return results
}

func UUIDsToInterfaces(input []uuid.UUID) []interface{} {
	results := make([]interface{}, len(input))
	for i, value := range input {
		results[i] = value.String()
	}
	return results
}

func RemoveTrimmedEmptyStrings(input []string) []string {
	results := make([]string, 0)
	for _, s := range input {
		if trimmed := strings.TrimSpace(s); len(trimmed) > 0 {
			results = append(results, trimmed)
		}
	}
	return results
}

func StringToUUIDMapValues(thing map[string]uuid.UUID) []uuid.UUID {
	results := make([]uuid.UUID, 0, len(thing))
	for _, value := range thing {
		results = append(results, value)
	}
	return results
}

func FilterStrings(values []string, includeFilter FilterFunc) []string {
	filtered := make([]string, 0, len(values))
	for _, value := range values {
		if includeFilter(value) {
			filtered = append(filtered, value)
		}
	}
	return filtered
}

func StringsIncludeFF(values []string) FilterFunc {
	return func(value string) bool {
		for _, v := range values {
			if v == value {
				return true
			}
		}
		return false
	}
}

func StringsExcludeFF(values []string) FilterFunc {
	return func(value string) bool {
		for _, v := range values {
			if v == value {
				return false
			}
		}
		return true
	}
}

func NewUniqueStrings() *UniqueStrings {
	return &UniqueStrings{
		Tracking: make(map[string]struct{}),
		Values:   make([]string, 0),
	}
}

func NewUniqueUUIDs() *UniqueUUIDs {
	return &UniqueUUIDs{
		Tracking: make(map[uuid.UUID]struct{}),
		Values:   make([]uuid.UUID, 0),
	}
}

func (u *UniqueStrings) Add(values ...string) {
	for _, value := range values {
		if _, ok := u.Tracking[value]; ok {
			continue
		}
		u.Tracking[value] = struct{}{}
		u.Values = append(u.Values, value)
	}
}

func (u *UniqueStrings) With(values ...string) *UniqueStrings {
	for _, value := range values {
		if _, ok := u.Tracking[value]; ok {
			continue
		}
		u.Tracking[value] = struct{}{}
		u.Values = append(u.Values, value)
	}
	return u
}

func (u *UniqueUUIDs) Add(values ...uuid.UUID) {
	for _, value := range values {
		if _, ok := u.Tracking[value]; ok {
			continue
		}
		u.Tracking[value] = struct{}{}
		u.Values = append(u.Values, value)
	}
}

func (u *UniqueUUIDs) With(values ...uuid.UUID) *UniqueUUIDs {
	for _, value := range values {
		if _, ok := u.Tracking[value]; ok {
			continue
		}
		u.Tracking[value] = struct{}{}
		u.Values = append(u.Values, value)
	}
	return u
}

func StringAppendIfMissing(slice []string, i string) []string {
	for _, ele := range slice {
		if ele == i {
			return slice
		}
	}
	return append(slice, i)
}
