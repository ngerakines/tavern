package common

import (
	"fmt"
)

func DomainPrefix(domain string) string {
	return fmt.Sprintf("https://%s/", domain)
}

func GroupActorURLPrefix(domain string) string {
	return fmt.Sprintf("https://%s/groups/", domain)
}

func ActorURLPrefix(domain string) string {
	return fmt.Sprintf("https://%s/users/", domain)
}

func GroupActorURL(domain string, name interface{}) string {
	return fmt.Sprintf("%s%s", GroupActorURLPrefix(domain), name)
}

func ActorURL(domain string, name interface{}) string {
	return fmt.Sprintf("%s%s", ActorURLPrefix(domain), name)
}

func ActivityURLPrefix(domain string) string {
	return fmt.Sprintf("https://%s/activity/", domain)
}

func ActivityURL(domain string, activityID interface{}) string {
	return fmt.Sprintf("%s%s", ActivityURLPrefix(domain), activityID)
}

func ObjectURLPrefix(domain string) string {
	return fmt.Sprintf("https://%s/object/", domain)
}

func ObjectURL(domain string, objectID interface{}) string {
	return fmt.Sprintf("%s%s", ObjectURLPrefix(domain), objectID)
}

func ObjectRepliesURL(domain string, objectID interface{}) string {
	return fmt.Sprintf("https://%s/object/%s/replies", domain, objectID)
}

func ObjectRepliesPageURL(domain string, objectID interface{}, page int) string {
	return fmt.Sprintf("https://%s/object/%s/replies?page=%d", domain, objectID, page)
}

func TagURL(domain string, objectID interface{}) string {
	return fmt.Sprintf("https://%s/tag/%s", domain, objectID)
}
