package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAllowDenyMatcher(t *testing.T) {
	type admt struct {
		source   string
		positive []string
		negative []string
	}
	samples := []admt{
		{
			source:   "mastodon.social",
			positive: []string{"mastodon.social"},
			negative: []string{"cdn.mastodon.social"},
		},
		{
			source:   "tavern.ngrok.io",
			positive: []string{"tavern.ngrok.io"},
			negative: []string{"cdn.tavern.ngrok.io", "ngrok.io"},
		},
		{
			source:   "*tavern.ngrok.io",
			positive: []string{"tavern.ngrok.io", "cdn.tavern.ngrok.io"},
			negative: []string{"ngrok.io"},
		},
		{
			source:   "cdn.*",
			positive: []string{"cdn.tavern.ngrok.io", "cdn.mastodon.social"},
			negative: []string{"east-cdn.mastodon.social"},
		},
	}
	for _, sample := range samples {
		m := NewAllowDenyMatcher(sample.source)
		for _, p := range sample.positive {
			assert.True(t, m.Match(p), "%s %d matches %s", sample.source, m.MatchType, p)
		}
		for _, n := range sample.negative {
			assert.False(t, m.Match(n), "%s %d matches %s", sample.source, m.MatchType, n)
		}
	}
}
