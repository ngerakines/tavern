package common

import (
	"io"
)

type NoOpReaderCloser struct {
	Reader io.Reader
}

func (rc NoOpReaderCloser) Read(p []byte) (n int, err error) {
	return rc.Reader.Read(p)
}

func (rc NoOpReaderCloser) Close() error {
	return nil
}

var _ io.ReadCloser = NoOpReaderCloser{}
