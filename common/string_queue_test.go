package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringQueue(t *testing.T) {
	q := NewStringQueue()
	assert.NoError(t, q.Add("a"))
	assert.NoError(t, q.Add("b"))
	assert.NoError(t, q.Add("c"))

	a, e1 := q.Take()
	assert.Equal(t, "a", a)
	assert.NoError(t, e1)

	b, e2 := q.Take()
	assert.Equal(t, "b", b)
	assert.NoError(t, e2)

	c, e3 := q.Take()
	assert.Equal(t, "c", c)
	assert.NoError(t, e3)

	d, e4 := q.Take()
	assert.Equal(t, "", d)
	assert.NoError(t, e4)
}
