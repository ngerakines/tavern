package common

import (
	"testing"
)

func TestAcceptHeaders_Match(t *testing.T) {
	type args struct {
		exact bool
		types []string
	}
	tests := []struct {
		name  string
		a     AcceptHeaders
		args  args
		want  string
		want1 string
		want2 bool
	}{
		{name: "a", a: ParseAccept("*/*"), args: args{exact: false, types: []string{"text/plain"}}, want: "*/*", want1: "text/plain", want2: true},
		{name: "b", a: ParseAccept("text/plain"), args: args{exact: false, types: []string{"text/plain"}}, want: "text/plain", want1: "text/plain", want2: true},
		{name: "c", a: ParseAccept("text/plain,text/*,*/*"), args: args{exact: false, types: []string{"text/plain"}}, want: "text/plain", want1: "text/plain", want2: true},
		{name: "d", a: ParseAccept("text/plain,text/*,*/*"), args: args{exact: false, types: []string{"text/html"}}, want: "text/*", want1: "text/html", want2: true},

		{name: "e", a: ParseAccept("text/plain"), args: args{exact: true, types: []string{"text/plain"}}, want: "text/plain", want1: "text/plain", want2: true},
		{name: "f", a: ParseAccept("text/plain,text/*,*/*"), args: args{exact: true, types: []string{"text/plain"}}, want: "text/plain", want1: "text/plain", want2: true},
		{name: "g", a: ParseAccept("*/*"), args: args{exact: true, types: []string{"text/plain"}}, want: "", want1: "", want2: false},
		{name: "h", a: ParseAccept("text/plain,text/*,*/*"), args: args{exact: true, types: []string{"text/html"}}, want: "", want1: "", want2: false},

		// {name: "a", a: ParseAccept("*/*"), args: args{exact: false, types: []string{"text/html"}}, want: true},
		// {name: "b", a: ParseAccept("text/plain"), args: args{exact: false, types: []string{"text/html"}}, want: true},
		// {name: "c", a: ParseAccept("text/plain,text/*,*/*"), args: args{exact: false, types: []string{"text/html"}}, want: true},
		//
		// {name: "d", a: ParseAccept("*/*"), args: args{exact: false, types: []string{"application/json"}}, want: true},
		// {name: "e", a: ParseAccept("text/plain"), args: args{exact: false, types: []string{"*/*"}}, want: true},
		// {name: "f", a: ParseAccept("text/plain,text/*,*/*"), args: args{exact: false, types: []string{"text/html"}}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2 := tt.a.Match(tt.args.exact, tt.args.types...)
			if got != tt.want {
				t.Errorf("Match() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Match() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("Match() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}
