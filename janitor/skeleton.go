package janitor

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v2"
)

var SkeletonCommand = cli.Command{
	Name:  "skeleton",
	Usage: "Generate a skeleton for ",
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:  "destination",
			Usage: "The location to write files to.",
			Value: "./",
		},
		&cli.StringFlag{
			Name:  "tag",
			Usage: "The container tag to use.",
			Value: "latest",
		},
		&cli.IntFlag{
			Name:  "web-port",
			Usage: "The port to run web on",
			Value: 9000,
		},
		&cli.StringSliceFlag{
			Name:     "domain",
			Usage:    "Set the domains of the sites.",
			Required: true,
		},
		&cli.StringSliceFlag{
			Name:     "web-name",
			Usage:    "The names of the sites.",
			Required: false,
			Value:    cli.NewStringSlice("web"),
		},
		&cli.StringSliceFlag{
			Name:     "web-user",
			Usage:    "The admin users of the sites. The email will be the @tvrn.dev suffix",
			Required: false,
		},
		&cli.BoolFlag{
			Name:  "pem",
			Usage: "Generate a PEM for each site.",
			Value: true,
		},
		&cli.BoolFlag{
			Name:  "mount-assets",
			Usage: "Mount the assets directory locally.",
			Value: false,
		},
		&cli.BoolFlag{
			Name:  "mount-db",
			Usage: "Mount the db directory locally.",
			Value: false,
		},
	},
	Action: skeletonCommandAction,
}

const (
	svgerPort     = 9100
	publisherPort = 9200
)

func skeletonCommandAction(cliCtx *cli.Context) error {

	domains := cliCtx.StringSlice("domain")
	webNames := cliCtx.StringSlice("web-name")

	if len(domains) != len(webNames) {
		return fmt.Errorf("equal number of web-name and domain values expected")
	}

	dbPassword := secretGen("database secret")

	dockerComposeConfig := make(map[string]interface{})
	services := make(map[string]interface{})

	dbServiceConfig, err := dockerComposeDB(cliCtx, dbPassword)
	if err != nil {
		return err
	}

	services["db"] = dbServiceConfig

	services["svger"] = map[string]interface{}{
		"restart":  "on-failure",
		"image":    "ngerakines/svger:1.1.0",
		"networks": []string{"internal_network"},
		"ports":    []string{fmt.Sprintf("%d:%d", svgerPort, svgerPort)},
		"environment": []string{
			fmt.Sprintf("PORT=%d", svgerPort),
		},
	}
	services["publisher"] = map[string]interface{}{
		"restart":  "on-failure",
		"image":    fmt.Sprintf("ngerakines/tavern:%s", cliCtx.String("tag")),
		"networks": []string{"internal_network", "external_network"},
		"command":  "publisher",
		"ports":    []string{fmt.Sprintf("%d:%d", publisherPort, publisherPort)},
		"environment": []string{
			fmt.Sprintf("LISTEN=0.0.0.0:%d", publisherPort),
			fmt.Sprintf("PUBLISHER_CALLBACK=http://%s:%d/webhooks/publisher", webNames[0], cliCtx.Int("web-port")),
		},
	}

	for i, webName := range webNames {

		webServiceConfig, err := dockerComposeWeb(cliCtx, i, webName, domains[i], dbPassword)
		if err != nil {
			return err
		}

		services[webName] = webServiceConfig
	}

	dockerComposeConfig["services"] = services
	dockerComposeConfig["version"] = "3"
	dockerComposeConfig["networks"] = map[string]interface{}{
		"external_network": map[string]interface{}{},
		"internal_network": map[string]interface{}{
			"internal": true,
		},
	}

	d, err := yaml.Marshal(&dockerComposeConfig)
	if err != nil {
		return err
	}

	var runFileContent strings.Builder
	runFileContent.WriteString("#!/bin/sh\n")
	runFileContent.WriteString("docker-compose -f docker-compose.yml up -d db svger publisher\n")
	runFileContent.WriteString("sleep 15\n")
	for _, name := range webNames {
		runFileContent.WriteString(fmt.Sprintf("docker-compose -f docker-compose.yml run %s migrate\n", name))
		runFileContent.WriteString("sleep 5\n")
	}
	for _, name := range webNames {
		user, email, err := webUser(cliCtx, name)
		if err != nil {
			return err
		}
		runFileContent.WriteString(fmt.Sprintf("docker-compose -f docker-compose.yml run %s init-service --pem /service.pem\n", name))
		runFileContent.WriteString(fmt.Sprintf("docker-compose -f docker-compose.yml run %s init-admin --admin-email=%s --admin-password=password --admin-name=%s\n", name, email, user))
	}
	runFileContent.WriteString("docker-compose -f docker-compose.yml up -d\n")
	runFileContent.WriteString("docker-compose -f docker-compose.yml logs --tail=50 -f\n")

	var restartFileContent strings.Builder
	restartFileContent.WriteString("#!/bin/sh\n")
	restartFileContent.WriteString("docker-compose -f docker-compose.yml up -d\n")
	restartFileContent.WriteString("docker-compose -f docker-compose.yml logs --tail=50 -f\n")

	if err = writeStringToFile(cliCtx.String("destination"), "run.sh", runFileContent.String()); err != nil {
		return err
	}

	if err = writeStringToFile(cliCtx.String("destination"), "restart.sh", restartFileContent.String()); err != nil {
		return err
	}

	if err = writeBytesToFile(cliCtx.String("destination"), "docker-compose.yml", d); err != nil {
		return err
	}

	for _, fileName := range []string{"run.sh", "restart.sh"} {
		if err := os.Chmod(filepath.Join(cliCtx.String("destination"), fileName), 0744); err != nil {
			return err
		}
	}

	return nil
}

func dockerComposeDB(cliCtx *cli.Context, dbPassword string) (map[string]interface{}, error) {
	volumes := make([]string, 0)

	initSqlFile := fmt.Sprintf("init.sql")

	volumes = append(volumes, "./init.sql:/docker-entrypoint-initdb.d/10-init.sql")

	if cliCtx.Bool("mount-db") {
		volumes = append(volumes, "./postgres:/var/lib/postgresql/data")
	}

	dbServiceConfig := make(map[string]interface{})
	dbServiceConfig["restart"] = "on-failure"
	dbServiceConfig["image"] = "postgres:12-alpine"
	dbServiceConfig["networks"] = []string{"internal_network"}
	dbServiceConfig["volumes"] = volumes
	dbServiceConfig["env_file"] = []string{
		fmt.Sprintf("./%s", "db.env"),
	}

	var dbEnvFileContent strings.Builder
	dbEnvFileContent.WriteString(fmt.Sprintf("POSTGRES_PASSWORD=%s\n", dbPassword))

	if err := writeStringToFile(cliCtx.String("destination"), "db.env", dbEnvFileContent.String()); err != nil {
		return nil, err
	}

	var initSqlFileContent strings.Builder
	initSqlFileContent.WriteString(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";` + "\n")
	for _, database := range cliCtx.StringSlice("web-name") {
		initSqlFileContent.WriteString(fmt.Sprintf("CREATE DATABASE %s;\n", database))
	}

	if err := writeStringToFile(cliCtx.String("destination"), initSqlFile, initSqlFileContent.String()); err != nil {
		return nil, err
	}

	return dbServiceConfig, nil
}

func dockerComposeWeb(cliCtx *cli.Context, pos int, name, domain, dbPassword string) (map[string]interface{}, error) {
	tag := cliCtx.String("tag")
	webPort := cliCtx.Int("web-port") + pos
	destination := cliCtx.String("destination")

	user, _, err := webUser(cliCtx, name)
	if err != nil {
		return nil, err
	}

	webSecret := secretGen(name, "web secret")

	webEnvFile := fmt.Sprintf("%s.env", name)

	volumes := make([]string, 0)
	if cliCtx.Bool("mount-assets") {
		volumes = append(volumes, fmt.Sprintf("./web_%d_assets:/assets", pos))
	}
	if cliCtx.Bool("pem") {
		volumes = append(volumes, fmt.Sprintf("./%s.pem:/service.pem", name))
	}

	webServiceConfig := make(map[string]interface{})
	webServiceConfig["restart"] = "on-failure"
	webServiceConfig["image"] = fmt.Sprintf("ngerakines/tavern:%s", tag)
	webServiceConfig["networks"] = []string{"internal_network", "external_network"}
	webServiceConfig["ports"] = []string{
		fmt.Sprintf("%d:%d", webPort, webPort),
	}
	webServiceConfig["depends_on"] = []string{"db", "svger", "publisher"}
	webServiceConfig["env_file"] = []string{
		fmt.Sprintf("./%s", webEnvFile),
	}
	webServiceConfig["volumes"] = volumes

	var webEnvFileContent strings.Builder
	for _, line := range []string{
		"ENABLE_SENTRY=false",
		"ENABLE_SVGER=true",
		"ENABLE_PUBLISHER=true",
		"ENABLE_GROUPS=true",
		fmt.Sprintf("SECRET=%s", webSecret),
		fmt.Sprintf("DOMAIN=%s", domain),
		fmt.Sprintf("DATABASE=postgresql://postgres:%s@db:5432/%s?sslmode=disable", dbPassword, name),
		fmt.Sprintf("SVGER=http://svger:%d/", svgerPort),
		fmt.Sprintf("LISTEN=0.0.0.0:%d", webPort),
		"ASSET_STORAGE_FILE_BASE=/assets/",
		"ASSET_STORAGE_REMOTE_DENY=*",
		"ALLOW_REPLY_COLLECTION_UPDATES=true",
		fmt.Sprintf("PUBLISHER=http://publisher:%d/", publisherPort),
		"ALLOW_OBJECT_FOLLOW=true",
		"ALLOW_AUTO_ACCEPT_FOLLOWERS=true",
		"ALLOW_INBOX_FORWARDING=true",
		"ENABLE_GROUPS=true",
		"ALLOW_AUTO_ACCEPT_GROUP_FOLLOWERS=true",
		"ALLOW_REMOTE_GROUP_FOLLOWERS=true",
		"DEFAULT_GROUP_MEMBER_ROLE=1",
		fmt.Sprintf("ADMIN_NAME=%s", user),
	} {
		webEnvFileContent.WriteString(line)
		webEnvFileContent.WriteString("\n")
	}

	if err := writeStringToFile(destination, webEnvFile, webEnvFileContent.String()); err != nil {
		return nil, err
	}

	if cliCtx.Bool("pem") {
		privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			return nil, err
		}

		n := privateKey.PublicKey.N.Bytes()
		e := big.NewInt(int64(privateKey.PublicKey.E)).Bytes()

		h := md5.New()
		h.Write(n)
		h.Write(e)
		fingerprint := hex.EncodeToString(h.Sum(nil))

		privateKeyBytes := x509.MarshalPKCS1PrivateKey(privateKey)
		var privateKeyBuffer bytes.Buffer
		if err := pem.Encode(&privateKeyBuffer, &pem.Block{
			Type:  "PRIVATE KEY",
			Bytes: privateKeyBytes,
			Headers: map[string]string{
				"id": fmt.Sprintf("https://%s/server#%s", domain, fingerprint),
			},
		}); err != nil {
			return nil, err
		}

		if err := writeBytesToFile(destination, fmt.Sprintf("%s.pem", name), privateKeyBuffer.Bytes()); err != nil {
			return nil, err
		}
	}

	return webServiceConfig, nil
}

func writeStringToFile(destination, fileName, content string) error {
	return writeBytesToFile(destination, fileName, []byte(content))
}

func writeBytesToFile(destination, fileName string, content []byte) error {
	initSqlLoc := filepath.Join(destination, fileName)
	return ioutil.WriteFile(initSqlLoc, content, 0644)
}

func secretGen(inputs ...string) string {
	h := md5.New()
	h.Write([]byte(time.Now().String()))
	for _, input := range inputs {
		h.Write([]byte(input))
	}
	return hex.EncodeToString(h.Sum(nil))
}

func webUser(cliCtx *cli.Context, webName string) (string, string, error) {
	domains := cliCtx.StringSlice("domain")
	webs := cliCtx.StringSlice("web-name")

	users := cliCtx.StringSlice("web-user")

	for i, web := range webs {
		if web == webName {
			if cliCtx.IsSet("web-user") {
				user := users[i]
				return user, fmt.Sprintf("%s@%s", user, domains[i]), nil
			}
			return "nick", fmt.Sprintf("nick@%s", domains[i]), nil
		}
	}

	return "", "", fmt.Errorf("invalid domain, web-name, and web-user config")
}
