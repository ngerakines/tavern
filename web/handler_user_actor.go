package web

import (
	"math"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) userActorInfo(c *gin.Context) {
	accepted := common.ParseAccept(c.GetHeader("Accept"))
	if accepted.MatchHTML(true) {
		h.viewUser(c)
		return
	}

	name := c.Param("name")

	user, err := h.storage.GetUserByName(c.Request.Context(), name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	userActor, err := h.storage.GetActor(c.Request.Context(), user.ActorID)
	if err != nil {
		h.hardFail(c, err)
		return
	}

	h.writeJSONLDProfile(c, http.StatusOK, userActor.Payload)
}

func (h handler) userActorFollowers(c *gin.Context) {
	name := c.Param("name")

	ctx := c.Request.Context()

	user, err := h.storage.GetUserByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	total, err := h.storage.CountFollowing(ctx, user.ID)
	if err != nil {
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	response := storage.EmptyPayload()
	response["@context"] = "https://www.w3.org/ns/activitystreams"

	actorID := storage.NewActorID(user.Name, h.domain)
	page := intParam(c, "page", 0)

	if page == 0 {

		response["id"] = actorID.Followers()
		response["type"] = "OrderedCollection"
		response["totalItems"] = total
		response["first"] = actorID.FollowersPage(1)

		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	offset := (page - 1) * 20

	actors, err := h.storage.ListAcceptedFollowers(ctx, user.ID, 20, offset)
	if err != nil {
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	response["id"] = actorID.FollowersPage(1)
	response["type"] = "OrderedCollectionPage"
	response["totalItems"] = total
	response["partOf"] = actorID.Followers()
	if offset < total {
		response["next"] = actorID.FollowersPage(page + 1)
	}
	if page > 1 {
		response["prev"] = actorID.FollowersPage(page - 1)
	}
	response["orderedItems"] = actors

	h.writeJSONLD(c, http.StatusOK, response)
}

func (h handler) userActorFollowing(c *gin.Context) {
	name := c.Param("name")

	ctx := c.Request.Context()

	user, err := h.storage.GetUserByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	total, err := h.storage.CountFollowing(ctx, user.ID)
	if err != nil {
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	response := storage.EmptyPayload()
	response["@context"] = "https://www.w3.org/ns/activitystreams"

	actorID := storage.NewActorID(user.Name, h.domain)
	page := intParam(c, "page", 0)

	if page == 0 {

		response["id"] = actorID.Following()
		response["type"] = "OrderedCollection"
		response["totalItems"] = total
		response["first"] = actorID.FollowingPage(1)

		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	offset := (page - 1) * 20

	actors, err := h.storage.ListAcceptedFollowing(ctx, user.ID, 20, offset)
	if err != nil {
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	response["id"] = actorID.FollowersPage(1)
	response["type"] = "OrderedCollectionPage"
	response["totalItems"] = total
	response["partOf"] = actorID.Following()
	if offset < total {
		response["next"] = actorID.FollowingPage(page + 1)
	}
	if page > 1 {
		response["prev"] = actorID.FollowingPage(page - 1)
	}
	response["orderedItems"] = actors

	h.writeJSONLD(c, http.StatusOK, response)
}

func (h handler) userActorOutbox(c *gin.Context) {
	ctx := c.Request.Context()

	name := c.Param("name")

	user, err := h.storage.GetUserByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	actorID := storage.NewActorID(user.Name, h.domain)

	page := intParam(c, "page", 0)
	limit := 50

	total, err := h.storage.CountObjectPayloadsInUserOutbox(ctx, user.ID)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}

	lastPage := 1
	if total > limit {
		lastPage = int(math.Ceil(float64(total) / float64(limit)))
	}

	response := storage.EmptyPayload()

	if page == 0 {

		response["@context"] = "https://www.w3.org/ns/activitystreams"
		response["id"] = actorID.Outbox()
		response["type"] = "OrderedCollection"
		response["totalItems"] = total

		if total > 0 {
			response["first"] = actorID.OutboxPage(1)
			response["last"] = actorID.OutboxPage(lastPage)
		}

		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	response["id"] = actorID.OutboxPage(page)
	response["type"] = "OrderedCollectionPage"
	response["totalItems"] = total
	response["partOf"] = actorID.Outbox()

	if total == 0 || page > lastPage {
		response["orderedItems"] = []interface{}{}
		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	objects, err := h.storage.ListObjectPayloadsInUserOutbox(ctx, user.ID, limit, (page-1)*limit)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}

	if len(objects) > 0 {
		response["orderedItems"] = objects
	}

	if page > 1 {
		response["prev"] = actorID.OutboxPage(page - 1)
	}
	if page < lastPage {
		response["next"] = actorID.OutboxPage(page + 1)
	}

	h.writeJSONLD(c, http.StatusOK, response)
}
