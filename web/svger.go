package web

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"

	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/g"
)

type SVGConverter interface {
	Convert(ctx context.Context, svg []byte) (io.ReadCloser, int64, error)
}

type SVGerClient struct {
	HTTPClient common.HTTPClient
	Endpoint   string
}

func DefaultSVGerClient(endpoint string, httpClient common.HTTPClient) SVGConverter {
	return SVGerClient{
		HTTPClient: httpClient,
		Endpoint:   endpoint,
	}
}

func (c SVGerClient) Convert(ctx context.Context, svg []byte) (io.ReadCloser, int64, error) {
	req, err := http.NewRequestWithContext(ctx, "POST", c.Endpoint, bytes.NewBuffer(svg))
	if err != nil {
		return nil, 0, err
	}
	req.Header.Set("Content-Type", "image/svg+xml")
	req.Header.Set("User-Agent", g.UserAgent())

	response, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, 0, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, 0, fmt.Errorf("unexpected response: %d", response.StatusCode)
	}
	return response.Body, response.ContentLength, nil
}
