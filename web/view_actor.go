package web

import (
	"fmt"
)

type actorLookup struct {
	domain string
	actors map[string]map[string]string
}

func (al actorLookup) Lookup(focus string, actorID string) string {
	switch focus {
	case "icon":
		actor, found := al.actors[actorID]
		if found {
			icon, found := actor["icon"]
			if found {
				return icon
			}
			username, foundA := actor["name"]
			actorDomain, foundB := actor["domain"]
			if foundA && foundB {
				return fmt.Sprintf("https://%s/avatar/png/%s/%s", al.domain, actorDomain, username)
			}
		}
		return fmt.Sprintf("https://%s/avatar/png/unknown/unknown", al.domain)
	case "name":
		actor, found := al.actors[actorID]
		if found {
			if at, ok := actor["at"]; ok {
				return at
			}
			if preferredUsername, ok := actor["preferred_username"]; ok {
				return preferredUsername
			}
		}
		return actorID
	default:
		return fmt.Sprintf("unknown key: %s", focus)
	}
}
