package web

import (
	"github.com/gin-contrib/sessions"

	"github.com/ngerakines/tavern/errors"
)

const (
	flashError   = "error"
	flashSuccess = "success"
	flashInfo    = "_flash"
)

type flashes struct {
	Success []interface{}
	Info    []interface{}
	Error   []interface{}
}

func getFlashes(session sessions.Session) flashes {
	return flashes{
		Success: session.Flashes(flashSuccess),
		Info:    session.Flashes(flashInfo),
		Error:   session.Flashes(flashError),
	}
}

func (f flashes) Show() bool {
	return len(f.Success) > 0 || len(f.Info) > 0 || len(f.Error) > 0
}

func appendFlashError(session sessions.Session, messages ...string) error {
	for _, message := range messages {
		session.AddFlash(message, flashError)
	}
	return errors.WrapCannotSaveSessionError(session.Save())
}

func appendFlashSuccess(session sessions.Session, message string) error {
	session.AddFlash(message, flashSuccess)
	return errors.WrapCannotSaveSessionError(session.Save())
}

func appendFlashInfo(session sessions.Session, message string) error {
	session.AddFlash(message, flashInfo)
	return errors.WrapCannotSaveSessionError(session.Save())
}
