package web

import (
	"bytes"
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/avatar"
)

func (h handler) avatarSVG(c *gin.Context) {
	svg, err := h.avatar(c)
	if err != nil {
		h.hardFail(c, err)
		return
	}
	svgReader := bytes.NewReader(svg)

	extraHeaders := map[string]string{
		"Cache-Control": "max-age=7200",
	}
	c.DataFromReader(http.StatusOK, svgReader.Size(), "image/svg+xml", svgReader, extraHeaders)
}

func (h handler) avatarPNG(c *gin.Context) {
	svg, err := h.avatar(c)
	if err != nil {
		h.hardFail(c, err)
		return
	}

	readerCloser, length, err := h.svgConverter.Convert(c.Request.Context(), svg)
	if err != nil {
		if err == context.Canceled {
			c.Status(http.StatusOK)
			return
		}
		h.hardFail(c, err)
		return
	}
	defer readerCloser.Close()

	extraHeaders := map[string]string{
		"Cache-Control": "max-age=7200",
	}
	c.DataFromReader(http.StatusOK, length, "image/png", readerCloser, extraHeaders)
}

func (h handler) avatar(c *gin.Context) ([]byte, error) {
	name := c.Param("name")
	domain := c.Param("domain")
	size := intParam(c, "size", 120)

	h.logger.Debug("avatar", zap.String("name", name), zap.String("domain", domain), zap.Int("size", size))

	if len(name) == 0 {
		name = domain
		domain = h.domain
	}

	if name == domain && name == "unknown" {
		svg := avatar.AvatarSVG("unknown", size, false)
		return []byte(svg), nil
	}

	exists, err := h.storage.ActorAliasSubjectExists(c.Request.Context(), fmt.Sprintf("acct:%s@%s", name, domain))
	if err != nil {
		return nil, err
	}
	if !exists {
		svg := avatar.AvatarSVG("unknown", size, false)
		return []byte(svg), nil
	}

	id := fmt.Sprintf("@%s@%s", name, domain)
	svg := avatar.AvatarSVG(id, size, domain == h.domain)
	return []byte(svg), nil
}
