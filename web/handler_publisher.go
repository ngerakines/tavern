package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type publisherEvent struct {
	Activity    string `json:"activity"`
	Destination string `json:"destination"`
	Status      string `json:"status"`
	Error       string `json:"error"`
}

func (h handler) publisherWebhook(c *gin.Context) {
	var e publisherEvent
	err := c.ShouldBindJSON(&e)
	if err != nil {
		h.logger.Warn("unable to parse publisher webhook event", zap.Error(err))
		c.Status(http.StatusOK)
	} else {
		h.logger.Debug("publisher webhook event received", zap.Reflect("event", e))
	}

	c.Status(http.StatusOK)
}
