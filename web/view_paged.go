package web

import (
	"math"
	"net/url"
	"strconv"
)

type paged struct {
	Page      int
	Pages     []pagedPage
	Total     int
	Limit     int
	Location  string
	PageCount int

	First    *pagedPage
	Previous *pagedPage
	Current  *pagedPage
	Next     *pagedPage
	Last     *pagedPage
}

type pagedPage struct {
	Current bool
	Label   string
	URL     string
}

func createPaged(limit, currentPage, total int, location string) (paged, error) {
	pageCount := int(math.Ceil(float64(total) / float64(limit)))

	u, err := url.Parse(location)
	if err != nil {
		return paged{}, err
	}

	var pagedPages []pagedPage

	var first *pagedPage
	var previous *pagedPage
	var next *pagedPage
	var last *pagedPage
	var current *pagedPage

	if currentPage > 1 {
		q := u.Query()
		q.Set("page", strconv.Itoa(1))
		u.RawQuery = q.Encode()
		first = &pagedPage{
			URL: u.String(),
		}
	}
	if currentPage-1 >= 1 {
		q := u.Query()
		q.Set("page", strconv.Itoa(currentPage-1))
		u.RawQuery = q.Encode()
		previous = &pagedPage{
			URL: u.String(),
		}
	}
	if currentPage+1 <= pageCount {
		q := u.Query()
		q.Set("page", strconv.Itoa(currentPage+1))
		u.RawQuery = q.Encode()
		next = &pagedPage{
			URL: u.String(),
		}
	}
	if currentPage < pageCount {
		q := u.Query()
		q.Set("page", strconv.Itoa(pageCount))
		u.RawQuery = q.Encode()
		last = &pagedPage{
			URL: u.String(),
		}
	}

	q := u.Query()
	q.Set("page", strconv.Itoa(currentPage))
	u.RawQuery = q.Encode()
	current = &pagedPage{
		Current: true,
		Label:   strconv.Itoa(currentPage),
		URL:     u.String(),
	}

	return paged{
		Location:  location,
		Page:      currentPage,
		PageCount: pageCount,
		Pages:     pagedPages,
		Limit:     limit,
		Total:     total,
		First:     first,
		Previous:  previous,
		Current:   current,
		Next:      next,
		Last:      last,
	}, nil
}
