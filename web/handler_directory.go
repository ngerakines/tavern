package web

import (
	"net/http"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/storage"
)

type groupDirectorySummary struct {
	Name        string
	ActorID     string
	MemberCount int
	CreatedAt   time.Time
}

func (h handler) groupDirectory(c *gin.Context) {
	session := sessions.Default(c)
	ctx := c.Request.Context()
	trans, transOK := c.Get("trans")
	if !transOK {
		panic("trans not found in context")
	}
	data := gin.H{
		"flashes": getFlashes(session),
		"Trans":   trans,
	}

	var total int
	var groups []groupDirectorySummary

	page := intParam(c, "page", 1)
	limit := 20

	txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
		var err error
		total, err = tx.RowCount(ctx, `SELECT COUNT(*) FROM groups WHERE directory_opt_in = $1`, true)
		if err != nil {
			return err
		}

		paginatedGroups, err := tx.ListGroupsPaginated(ctx, limit, (page-1)*limit)
		if err != nil {
			return err
		}

		if len(paginatedGroups) > 0 {
			groupActorRowIDs := make([]uuid.UUID, len(paginatedGroups))
			for i, pg := range paginatedGroups {
				groupActorRowIDs[i] = pg.ID
			}

			groupCounts, err := tx.CountGroupsMembers(ctx, groupActorRowIDs)
			if err != nil {
				return err
			}
			keyedGroupCounts := storage.CountMap(groupCounts)

			for _, pg := range paginatedGroups {
				groups = append(groups, groupDirectorySummary{
					Name:        pg.Name,
					ActorID:     pg.ActorID,
					MemberCount: keyedGroupCounts[pg.ID.String()],
					CreatedAt:   pg.CreatedAt,
				})
			}
		}

		return nil
	})
	if txErr != nil {
		h.hardFail(c, txErr)
		return
	}

	data["groups"] = groups

	paged, err := createPaged(limit, page, total, h.url("group_directory"))
	if err == nil {
		data["paged"] = paged
	}

	c.HTML(http.StatusOK, "group_directory", data)
}
