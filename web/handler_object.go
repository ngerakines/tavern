package web

import (
	"fmt"
	"math"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) getObject(c *gin.Context) {
	accepted := common.ParseAccept(c.GetHeader("Accept"))
	if accepted.MatchHTML(true) {
		h.viewObject(c)
		return
	}

	objectUUID, err := uuid.FromString(c.Param("object"))
	if err != nil {
		h.notFoundJSON(c, err)
		return
	}

	objectID := fmt.Sprintf("https://%s/object/%s", h.domain, objectUUID)
	objectPayload, err := h.storage.ObjectPayloadByObjectID(c.Request.Context(), objectID)
	if err != nil {
		h.notFoundJSON(c, errors.NewNotFoundError(err))
		return
	}

	objectType, hasObjectType := storage.JSONString(objectPayload, "type")
	if hasObjectType && objectType == "Tombstone" {
		h.writeJSONLD(c, http.StatusOK, objectPayload)
		return
	}

	var destinations []string
	to, hasTo := storage.JSONStrings(objectPayload, "to")
	if hasTo {
		destinations = append(destinations, to...)
	}
	cc, hasCc := storage.JSONStrings(objectPayload, "cc")
	if hasCc {
		destinations = append(destinations, cc...)
	}

	isPublic := false
	for _, d := range destinations {
		if d == "https://www.w3.org/ns/activitystreams#Public" {
			isPublic = true
			break
		}
	}
	if !isPublic {
		h.notFoundJSON(c, errors.NewNotFoundError(fmt.Errorf("activity was not made public")))
		return
	}

	h.writeJSONLD(c, http.StatusOK, objectPayload)
}

func (h handler) getObjectReplies(c *gin.Context) {
	ctx := c.Request.Context()

	objectID := common.ObjectURL(h.domain, uuid.FromStringOrNil(c.Param("object")))
	objectRowID, err := h.storage.ObjectRowIDForObjectID(ctx, objectID)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}

	page := intParam(c, "page", 0)
	limit := 50

	total, err := h.storage.CountObjectPayloadsInObjectReplies(ctx, objectRowID)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}

	lastPage := 1
	if total > limit {
		lastPage = int(math.Ceil(float64(total) / float64(limit)))
	}

	response := storage.EmptyPayload()

	if page == 0 {

		response["@context"] = "https://www.w3.org/ns/activitystreams"
		response["id"] = common.ObjectRepliesURL(h.domain, objectRowID)
		response["type"] = "OrderedCollection"
		response["totalItems"] = total

		if total > 0 {
			response["first"] = common.ObjectRepliesPageURL(h.domain, objectRowID, 1)
			response["last"] = common.ObjectRepliesPageURL(h.domain, objectRowID, lastPage)
		}

		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	response["id"] = common.ObjectRepliesPageURL(h.domain, objectRowID, page)
	response["type"] = "OrderedCollectionPage"
	response["totalItems"] = total
	response["partOf"] = common.ObjectRepliesURL(h.domain, objectRowID)

	if total == 0 || page > lastPage {
		response["orderedItems"] = []interface{}{}
		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	objects, err := h.storage.ListObjectPayloadsInObjectReplies(ctx, objectRowID, limit, (page-1)*limit)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}

	if len(objects) > 0 {
		response["orderedItems"] = objects
	}

	if page > 1 {
		response["prev"] = common.ObjectRepliesPageURL(h.domain, objectRowID, page-1)
	}
	if page < lastPage {
		response["next"] = common.ObjectRepliesPageURL(h.domain, objectRowID, page+1)
	}

	h.writeJSONLD(c, http.StatusOK, response)
}
