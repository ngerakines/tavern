package web

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/ngerakines/tavern/common"
)

func (h handler) tavernAbout(c *gin.Context) {
	users, err := h.storage.CountUsers(c.Request.Context())
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	activities, err := h.storage.CountObjectEvents(c.Request.Context())
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	openRegistrations := false
	c.HTML(http.StatusOK, "tavern_about", gin.H{
		"users":              users,
		"activities":         activities,
		"open_registrations": openRegistrations,
		"domain":             h.domain,
		"admin_user":         h.adminUser,
		"admin_user_link":    common.ActorURL(h.domain, h.adminUser),
	})
}

func (h handler) tavernTerms(c *gin.Context) {
	c.HTML(http.StatusOK, "tavern_terms", gin.H{})
}

func (h handler) tavernUsage(c *gin.Context) {
	c.HTML(http.StatusOK, "tavern_usage", gin.H{})
}
