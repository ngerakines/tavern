package web

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday/v2"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) viewGroup(c *gin.Context) {
	session := sessions.Default(c)
	ctx := c.Request.Context()
	trans, transOK := c.Get("trans")
	if !transOK {
		panic("trans not found in context")
	}
	data := gin.H{
		"flashes": getFlashes(session),
		"Trans":   trans,
	}

	txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
		authenticated := true

		localUser, err := tx.GetUserBySession(ctx, session)
		if err != nil {
			if !errors.Is(err, errors.NewNotFoundError(nil)) {
				return err
			}
			authenticated = false
		}

		name := c.Param("name")

		group, err := tx.GetGroupByName(ctx, name)
		if err != nil {
			return err
		}

		groupActor, err := tx.GetActor(ctx, group.ActorID)
		if err != nil {
			return err
		}

		data["group"] = group
		data["group_actor"] = groupActor
		data["domain"] = h.domain

		totalMembers, err := tx.CountGroupMembers(ctx, group.ID)
		if err != nil {
			return err
		}
		data["members_total"] = totalMembers

		data["authenticated"] = authenticated
		data["owner"] = false

		if !authenticated {
			return nil
		}

		data["user"] = localUser
		data["owner"] = group.OwnerID == localUser.ID

		userActor, err := tx.GetActor(ctx, localUser.ActorID)
		if err != nil {
			return err
		}
		data["user_actor"] = userActor

		members, err := tx.GroupMemberActorsForGroupActorID(ctx, group.ActorID)
		if err != nil {
			return err
		}

		data["members"] = members

		return nil
	})
	if txErr != nil {
		h.hardFail(c, txErr)
		return
	}

	c.HTML(http.StatusOK, "group_profile", data)
}

func (h handler) configureGroup(c *gin.Context) {
	user, _, cont := h.loggedInAPI(c, true)
	if !cont {
		return
	}

	ctx := c.Request.Context()

	group, err := h.storage.GetGroupByName(ctx, c.Param("name"))
	if err != nil {
		h.hardFail(c, err)
		return
	}
	if group.OwnerID != user.ID {
		h.hardFail(c, err)
		return
	}

	groupActor, err := h.storage.GetActor(ctx, group.ActorID)
	if err != nil {
		h.hardFail(c, err)
		return
	}

	action := c.PostForm("action")
	switch action {
	case "update_about":
		txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
			unsafe := blackfriday.Run([]byte(c.PostForm("about")))
			html := bluemonday.UGCPolicy().SanitizeBytes(unsafe)

			err := tx.UpdateGroupAbout(ctx, group.ID, string(html))
			if err != nil {
				return err
			}
			groupPayload := groupActor.Payload
			groupPayload["summary"] = string(html)

			return tx.UpdateActorPayload(ctx, group.ActorID, groupPayload)
		})
		if txErr != nil {
			h.flashErrorOrFail(c, h.url("group", group.Name), txErr)
			return
		}
	case "update_auto_accept":
		txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
			autoFollow, _ := strconv.ParseBool(c.PostForm("auto_follow"))
			return tx.UpdateGroupAcceptFollowers(ctx, group.ID, autoFollow)
		})
		if txErr != nil {
			h.flashErrorOrFail(c, h.url("group", group.Name), txErr)
			return
		}
	case "update_default_role":
		h.logger.Debug("update_default_role", zap.String("value", c.PostForm("default_role")))
		txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
			defaultMemberRole, err := strconv.Atoi(c.PostForm("default_role"))
			if err != nil {
				return err
			}
			switch storage.GroupRole(defaultMemberRole) {
			case storage.GroupViewer, storage.GroupMember, storage.GroupOwner:
			default:
				return fmt.Errorf("invalid group role: %d", defaultMemberRole)
			}

			return tx.UpdateGroupDefaultRole(ctx, group.ID, storage.GroupRole(defaultMemberRole))
		})
		if txErr != nil {
			h.flashErrorOrFail(c, h.url("group", group.Name), txErr)
			return
		}
	case "update_allow_remote":
		txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
			allowRemote, _ := strconv.ParseBool(c.PostForm("allow_remote"))
			return tx.UpdateGroupAllowRemote(ctx, group.ID, allowRemote)
		})
		if txErr != nil {
			h.flashErrorOrFail(c, h.url("group", group.Name), txErr)
			return
		}
	case "update_directory":
		txErr := storage.TransactionalStorage(ctx, h.storage, func(tx storage.Storage) error {
			directoryOptIn, _ := strconv.ParseBool(c.PostForm("directory"))
			return tx.UpdateGroupDirectoryOptIn(ctx, group.ID, directoryOptIn)
		})
		if txErr != nil {
			h.flashErrorOrFail(c, h.url("group", group.Name), txErr)
			return
		}
	}

	c.Redirect(http.StatusFound, h.url("group", group.Name))
}
