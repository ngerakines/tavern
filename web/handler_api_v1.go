package web

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/ngerakines/tavern/g"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) apiV1Instance(c *gin.Context) {
	ctx := c.Request.Context()
	user, err := h.storage.GetUserByName(ctx, h.adminUser)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	followers, err := h.storage.CountUserFollowers(ctx, user.ID)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	following, err := h.storage.CountUserFollowing(ctx, user.ID)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	users, err := h.storage.CountUsers(ctx)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	activities, err := h.storage.CountObjectEvents(ctx)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	userActivities, err := h.storage.CountUserObjectEvents(ctx, user.ID)
	if err != nil {
		h.internalServerErrorJSON(c, err)
		return
	}
	c.JSON(http.StatusOK, map[string]interface{}{
		"uri":               h.domain,
		"title":             "Tavern",
		"short_description": "An ActivityPub server.",
		"description":       "An ActivityPub server.",
		"email":             fmt.Sprintf("admin@%s", h.domain),
		"version":           g.Version(),
		"urls":              map[string]interface{}{},
		"stats": map[string]interface{}{
			"user_count":   users,
			"status_count": activities,
			"domain_count": 1,
		},
		"thumbnail":         fmt.Sprintf("https://%s/public/server_thumbnail.png", h.domain),
		"languages":         []string{"en"},
		"registrations":     false,
		"approval_required": true,
		"contact_account": map[string]interface{}{
			"id":              user.ID.String(),
			"username":        user.Name,
			"acct":            user.Name,
			"display_name":    user.DisplayName,
			"locked":          false,
			"bot":             false,
			"discoverable":    true,
			"group":           false,
			"created_at":      user.CreatedAt,
			"note":            user.About,
			"url":             storage.NewActorID(user.Name, h.domain),
			"avatar":          fmt.Sprintf("https://%s/avatars/%s", h.domain, user.Name),
			"avatar_static":   fmt.Sprintf("https://%s/avatars/%s", h.domain, user.Name),
			"followers_count": followers,
			"following_count": following,
			"statuses_count":  userActivities,
			"last_status_at":  user.LastAuthAt.Format("2006-01-02"),
			"fields":          []string{},
		},
	})
}

func (h handler) apiV1InstancePeers(c *gin.Context) {
	c.JSON(http.StatusOK, []string{})
}
