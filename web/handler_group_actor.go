package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) groupActorInfo(c *gin.Context) {
	accepted := common.ParseAccept(c.GetHeader("Accept"))
	if accepted.MatchHTML(true) {
		h.viewGroup(c)
		return
	}

	name := c.Param("name")

	group, err := h.storage.GetGroupByName(c.Request.Context(), name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	groupActor, err := h.storage.GetActor(c.Request.Context(), group.ActorID)
	if err != nil {
		h.hardFail(c, err)
		return
	}

	h.writeJSONLDProfile(c, http.StatusOK, groupActor.Payload)
}

func (h handler) groupActorFollowers(c *gin.Context) {
	name := c.Param("name")

	ctx := c.Request.Context()

	group, err := h.storage.GetGroupByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	total, err := h.storage.CountGroupMembers(ctx, group.ActorID)
	if err != nil {
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	response := storage.EmptyPayload()
	response["@context"] = "https://www.w3.org/ns/activitystreams"

	actorID := storage.ActorID(common.GroupActorURL(h.domain, group.Name))
	page := intParam(c, "page", 0)

	if page == 0 {

		response["id"] = actorID.Followers()
		response["type"] = "OrderedCollection"
		response["totalItems"] = total
		response["first"] = actorID.FollowersPage(1)

		h.writeJSONLD(c, http.StatusOK, response)
		return
	}

	offset := (page - 1) * 20

	actors, err := h.storage.GroupMemberActorIDs(ctx, group.ActorID)
	if err != nil {
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	response["id"] = actorID.FollowersPage(1)
	response["type"] = "OrderedCollectionPage"
	response["totalItems"] = total
	response["partOf"] = actorID.Followers()
	if offset < total {
		response["next"] = actorID.FollowersPage(page + 1)
	}
	if page > 1 {
		response["prev"] = actorID.FollowersPage(page - 1)
	}
	response["orderedItems"] = actors

	h.writeJSONLD(c, http.StatusOK, response)
}

func (h handler) groupActorFollowing(c *gin.Context) {
	name := c.Param("name")

	ctx := c.Request.Context()

	group, err := h.storage.GetGroupByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	actorID := storage.ActorID(common.GroupActorURL(h.domain, group.Name))

	response := storage.EmptyPayload()
	response["@context"] = "https://www.w3.org/ns/activitystreams"
	response["id"] = actorID.Following()
	response["type"] = "OrderedCollection"
	response["totalItems"] = 0

	h.writeJSONLD(c, http.StatusOK, response)
}

func (h handler) groupActorOutbox(c *gin.Context) {
	ctx := c.Request.Context()

	name := c.Param("name")

	group, err := h.storage.GetGroupByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, nil)
			return
		}
		h.internalServerErrorJSON(c, err, zap.String("name", name))
		return
	}

	actorID := storage.ActorID(common.GroupActorURL(h.domain, group.Name))

	response := storage.EmptyPayload()
	response["@context"] = "https://www.w3.org/ns/activitystreams"
	response["id"] = actorID.Outbox()
	response["type"] = "OrderedCollection"
	response["totalItems"] = 0

	h.writeJSONLD(c, http.StatusOK, response)
}
