package web

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) getActivity(c *gin.Context) {
	activityUUID, err := uuid.FromString(c.Param("activity"))
	if err != nil {
		h.notFoundJSON(c, err)
		return
	}

	ctx := c.Request.Context()

	activityID := fmt.Sprintf("https://%s/activity/%s", h.domain, activityUUID)

	activityPayload, err := h.storage.ObjectEventPayloadByActivityID(ctx, activityID)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, errors.NewNotFoundError(err))
			return
		}
		h.internalServerErrorJSON(c, err)
		return
	}

	destinations := storage.CollectJSONDeepStrings(activityPayload,
		[]string{"to"}, []string{"cc"}, []string{"object", "to"}, []string{"object", "cc"})

	isPublic := false
	for _, d := range destinations {
		if d == "https://www.w3.org/ns/activitystreams#Public" {
			isPublic = true
			break
		}
	}
	if !isPublic {
		h.notFoundJSON(c, errors.NewNotFoundError(fmt.Errorf("activity was not public")))
		return
	}

	h.writeJSONLD(c, http.StatusOK, activityPayload)
}
