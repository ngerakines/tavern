package web

import (
	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"

	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) viewObject(c *gin.Context) {
	meta := map[string]interface{}{
		"feed_template": "object",
	}

	ctx := c.Request.Context()

	h.displayObjectFeed(c, false, meta, func(user *storage.User) (i int, payloads []storage.Payload, err error) {

		objectRowIDs := common.NewUniqueUUIDs()

		objectRowID, err := h.storage.ObjectRowIDForObjectID(ctx, common.ObjectURL(h.domain, c.Param("object")))
		if err != nil {
			h.hardFail(c, err)
			return
		}

		objectRowIDs.Add(objectRowID)

		parentObjectID, err := h.storage.ParentObjectID(ctx, objectRowID)
		if err != nil && !errors.Is(err, errors.NewNotFoundError(nil)) {
			h.hardFail(c, err)
			return
		}

		if parentObjectID != uuid.Nil {
			objectRowIDs.Add(parentObjectID)
		}

		objectChildren, err := h.storage.ObjectChildrenByObjectID(ctx, objectRowID)
		if err != nil {
			h.hardFail(c, err)
			return
		}
		for k, v := range objectChildren {
			objectRowIDs.Add(k)
			objectRowIDs.Add(v...)
		}

		refs, err := h.storage.ObjectPayloads(ctx, objectRowIDs.Values)
		if err != nil {
			h.hardFail(c, err)
			return
		}

		total := len(refs)
		return total, refs, nil
	})
}
