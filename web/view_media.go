package web

import (
	"context"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/ngerakines/tavern/storage"
)

type mediaLookup struct {
	domain   string
	payloads map[string]string
}

func (ml *mediaLookup) Lookup(size string, href string) string {
	data, ok := ml.payloads[fmt.Sprintf("%s,%s", size, href)]
	if ok {
		return data
	}
	return fmt.Sprintf("https://%s/asset/placeholder/%s", ml.domain, size)
}

func (ml *mediaLookup) load(ctx context.Context, s storage.Storage, media []viewFeedMedia) error {
	domainPrefix := fmt.Sprintf("https://%s/asset/image/", ml.domain)
	for _, href := range media {
		if strings.HasPrefix(href.URL, domainPrefix) {
			checksum := strings.TrimPrefix(href.URL, domainPrefix)
			ml.payloads[fmt.Sprintf("image,%s", href.URL)] = fmt.Sprintf("https://%s/asset/image/%s", ml.domain, checksum)
			ml.payloads[fmt.Sprintf("blurhash,%s", href.URL)] = fmt.Sprintf("https://%s/asset/blurhash/%s", ml.domain, hex.EncodeToString([]byte(href.Blurhash)))
			continue
		}
		imgs, err := s.GetImagesByAlias(ctx, []string{href.URL})
		if err != nil {
			return err
		}
		if len(imgs) > 0 {
			ml.payloads[fmt.Sprintf("image,%s", href.URL)] = fmt.Sprintf("https://%s/asset/image/%s", ml.domain, imgs[0].Checksum)
			ml.payloads[fmt.Sprintf("blurhash,%s", href.URL)] = fmt.Sprintf("https://%s/asset/blurhash/%s", ml.domain, hex.EncodeToString([]byte(href.Blurhash)))
			continue
		}
		ml.payloads[fmt.Sprintf("image,%s", href.URL)] = href.URL
		ml.payloads[fmt.Sprintf("blurhash,%s", href.URL)] = fmt.Sprintf("https://%s/asset/blurhash/%s", ml.domain, hex.EncodeToString([]byte(href.Blurhash)))
	}
	return nil
}
