package web

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func (h handler) utilities(c *gin.Context) {
	data, _, _, cont := h.loggedIn(c, true)
	if !cont {
		return
	}

	c.HTML(http.StatusOK, "utilities", data)
}

func (h handler) utilitiesWebfinger(c *gin.Context) {
	_, _, _, cont := h.loggedIn(c, true)
	if !cont {
		return
	}

	actor := c.PostForm("actor")
	err := h.webFingerQueue.Add(actor)
	if err != nil {
		h.logger.Error("unable to add actor to web finger queue", zap.String("actor", actor))
		h.flashErrorOrFail(c, h.url("utilities"), fmt.Errorf("unable to add actor to web finger queue"))
		return
	}

	h.flashSuccessOrFail(c, h.url("utilities"), "actor queued")
}

func (h handler) utilitiesCrawl(c *gin.Context) {
	_, _, _, cont := h.loggedIn(c, true)
	if !cont {
		return
	}

	activity := c.PostForm("activity")
	err := h.crawlQueue.Add(activity)
	if err != nil {
		h.logger.Error("unable to add activity to crawl queue", zap.String("activity", activity))
		h.flashErrorOrFail(c, h.url("utilities"), fmt.Errorf("unable to add activity to crawl queue"))
		return
	}

	h.flashSuccessOrFail(c, h.url("utilities"), "activity queued")
}
