package web

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/errors"
	"github.com/ngerakines/tavern/storage"
)

func (h handler) viewUser(c *gin.Context) {
	session := sessions.Default(c)
	ctx := c.Request.Context()

	data := gin.H{
		"flashes": getFlashes(session),
	}

	authenticated := true
	localUser, err := h.storage.GetUserBySession(ctx, session)
	if err != nil {
		if !errors.Is(err, errors.NewNotFoundError(nil)) {
			h.internalServerErrorJSON(c, err)
			return
		}
		authenticated = false
	}

	data["authenticated"] = authenticated
	data["user"] = localUser

	name := c.Param("name")

	viewUser, err := h.storage.GetUserByName(ctx, name)
	if err != nil {
		if errors.Is(err, errors.NewNotFoundError(nil)) {
			h.notFoundJSON(c, err)
			return
		}
		h.internalServerErrorJSON(c, err)
		return
	}

	viewUserActor, err := h.storage.GetActor(ctx, viewUser.ActorID)
	if err != nil {
		h.hardFail(c, err)
		return
	}

	data["view_user"] = viewUser
	data["view_user_actor"] = viewUserActor
	data["domain"] = h.domain
	data["feed_template"] = "profile"
	data["self_url"] = h.url("profile", viewUser.Name)

	page := intParam(c, "page", 1)
	limit := 20

	h.displayObjectFeed(c, false, data, func(user *storage.User) (i int, payloads []storage.Payload, err error) {
		total, err := h.storage.CountObjectPayloadsInUserOutbox(ctx, viewUser.ID)
		if err != nil {
			return 0, nil, err
		}
		h.logger.Debug("counted objects in feed", zap.Int("total", total))
		objects, err := h.storage.ListObjectPayloadsInUserOutbox(ctx, viewUser.ID, limit, (page-1)*limit)
		if err != nil {
			return 0, nil, err
		}
		h.logger.Debug("queried objects", zap.Int("count", len(objects)))

		return total, objects, nil
	})
}
