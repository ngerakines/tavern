package asset

import (
	"context"
	"fmt"
	"runtime"

	"github.com/getsentry/sentry-go"
	"github.com/urfave/cli/v2"
	"go.uber.org/zap"

	"github.com/ngerakines/tavern/common"
	"github.com/ngerakines/tavern/config"
	"github.com/ngerakines/tavern/g"
	"github.com/ngerakines/tavern/storage"
)

var Command = cli.Command{
	Name:  "file",
	Usage: "Prepare a remote file",
	Flags: []cli.Flag{
		&config.EnvironmentFlag,
		&config.ListenFlag,
		&config.DomainFlag,
		&config.DatabaseFlag,
	},
	Action: fileCommandAction,
}

func fileCommandAction(cliCtx *cli.Context) error {
	logger, err := config.Logger(cliCtx)
	if err != nil {
		return err
	}

	domain := cliCtx.String("domain")
	siteBase := fmt.Sprintf("https://%s", domain)

	logger.Info("Starting",
		zap.String("command", cliCtx.Command.Name),
		zap.String("GOOS", runtime.GOOS),
		zap.String("site", siteBase),
		zap.String("env", cliCtx.String("environment")))

	sentryConfig, err := config.NewSentryConfig(cliCtx)
	if err != nil {
		return err
	}

	if sentryConfig.Enabled {
		err = sentry.Init(sentry.ClientOptions{
			Dsn:         sentryConfig.Key,
			Environment: cliCtx.String("environment"),
			Release:     fmt.Sprintf("%s-%s", g.Release, g.GitCommit),
		})
		if err != nil {
			return err
		}
		sentry.ConfigureScope(func(scope *sentry.Scope) {
			scope.SetTags(map[string]string{"container": "server"})
		})
		defer sentry.Recover()
	}

	db, dbClose, err := config.DB(cliCtx, logger)
	if err != nil {
		return err
	}
	defer dbClose()

	s := storage.DefaultStorage(storage.LoggingSQLDriver{Driver: db, Logger: logger})

	a := Agent{
		AssetStorage: fileStorage{Base: "./assets/"},
		DataStorage:  s,
		HTTPClient:   common.DefaultHTTPClient(),
	}

	ctx := context.Background()

	for _, arg := range cliCtx.Args().Slice() {
		_, err := a.HandleImage(ctx, arg)
		if err != nil {
			logger.Error("unable to process image", zap.String("image", arg), zap.Error(err))
			return err
		}
	}

	return nil
}
