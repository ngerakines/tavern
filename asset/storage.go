package asset

import (
	"context"
	"io"
)

type Storage interface {
	Close() error
	Create(ctx context.Context, location string, reader io.Reader) (string, error)
	Delete(ctx context.Context, location string) error
	Exists(ctx context.Context, location string) (bool, error)
	Read(ctx context.Context, location string) (io.ReadCloser, error)
	Upload(ctx context.Context, checksum string, source string) (string, error)
}
