# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1]

### Added

* Added support for uploading duplicate files.

### Fixed

* Fixed bug in uploads.

## [0.2.0]

### Added

* [#21 Groups](https://gitlab.com/ngerakines/tavern/-/issues/21)

  The groups feature has been implemented to allow for multiple types (open, broadcast, private, etc.) of groups to exist within Tavern. See [FEDERATION.md](FEDERATION.md) for Activity Pub implementation details.
  
  Groups are special actors that share the same namespace as users, have their own profiles ([#68](https://gitlab.com/ngerakines/tavern/-/issues/68)), and can be browsed via a directory ([#67](https://gitlab.com/ngerakines/tavern/-/issues/67)).

* Group invitations allow group members (manager level) to invite others to the group.
* Allow/Deny matching for inbox activity with server and local scope. This allows the server to implement blank allow/deny matching, as well as user or group allow/deny matching.
* Added migrations to create non-empty checks for varchar fields.
* Add init-service command to create a service actor specific to the instance.
* Added a start-up check for the service actor.
  
  **Important**: Without the service actor record, the server will not start up.
* Added user inbox handling of object likes.
* Added content warnings if object attribute "sensitive" exists and is true, and if summary is not empty.
* Added tavern version to footer.

### Changed

* Changed link in user agent.
* Added metrics collector storage driver to expose query metrics.
* Renamed the init command to init-admin to make room for initializing the service actor.
* Updated documentation reflecting the addition of the service actor.
* Feed views (recent, local, mine, user, and object) have improved nested object views.
* Updated templates to improve automated test support.
* Updated skeleton command in janitor package to improve automated test support.

### Fixed

* Fixed bug in view object where inReplyTo author link and name was not displayed.
* Fixed missing sentry flag references in web command.
* Fixed bug in init-admin where key id was not set correctly.
* Fixed bug in announce due to code being removed when it shouldn't have been.

## [0.1.2]

### Changed

* Updated pagination template to add first, previous, next, and last links.

### Fixed

* Fixed bug where wrapped uuid query was not returning an expected not found error.

## [0.1.1]

### Fixed

* Fixed bug in bad SQL query in storage.ActorsSubscribedToObject.

## [0.1.0]

### Added

* [#35 Conversation replies](https://gitlab.com/ngerakines/tavern/-/issues/35)

  This implements Note subscriptions through `Follow/Note` and `Undo/Follow/Note` activity processing. See the [FEDERATION.md](FEDERATION.md) file for more information on how inbox forwarding is implemented for `Create/Note`, `Announce/Note`, and `Delete/Note` activities.

* [#66 Broadcast agent](https://gitlab.com/ngerakines/tavern/-/issues/66)

  Issue #66 introduces the `publisher` command that allows for async processing of publish operations.

### Fixed

* Fixed a bug where generated error translations file was incorrectly named.

## [0.1.0-rc3]

### Added

* Added examples/linode/README.md that walks through installing a tavern cluster on https://www.linode.com/.

### Fixed

* Fixed bug in image uploading where incorrect file path is used.
* [#63 Deletes show up blank in feed](https://gitlab.com/ngerakines/tavern/-/issues/63)
* [#64 Replies by actors who are not followed are being ignored.](https://gitlab.com/ngerakines/tavern/-/issues/64)
* [#65 User profile pages are no longer publicly visible](https://gitlab.com/ngerakines/tavern/-/issues/65)

## [0.1.0-rc2]

### Added

* Added docker-compose file and quick-start information to README.
* Added CORS defaults to gin router.

### Changed

* Database migrations are now bundled in executable when built with the "prod" tag. This is the default for docker container builds.
* Translations are now bundled in executable when built with the "prod" tag. This is the default for docker container builds.
* Added links to about, terms, and usage to footer.
* Updated actor url to display database-stored payload instead of generating it with each request.

### Fixed

* Fixed bug in actor URLs not matching correct paths.
* Fixed bug in start command where user and actor was not created correctly.
* Fixed bug where incorrect actor key identifier was provided.
* Fixed bug where incorrect template was displayed for fresh users with no feed content.
* [#53 About page](https://gitlab.com/ngerakines/tavern/-/issues/53)
* [#54 Terms page](https://gitlab.com/ngerakines/tavern/-/issues/54)
* [#55 Usage page](https://gitlab.com/ngerakines/tavern/-/issues/55)

### Removed

* Removed templates, translations, and migrations from docker container.

## [0.1.0-rc1]

Project created

### Added

- Implemented site infrastructure using [Golang](https://golang.org/) and [Gin](https://github.com/gin-gonic/gin)
- Implemented actor, object, activity, and network data structures and Postgres-backed storage layer.
- Implemented webfinger, actor, object, and activity Activity Pub resources
- Implemented feed, my feed, local feed, user feed, compose note, and advanced compose note UI resources
- Implemented sign-in, sign-out, settings, and utilities UI resources
- Implemented translations framework for site localization
- Implemented 
- Added README.md
- Added FEDERATION.md
- Added CONTRIBUTING.md
- Added CHANGELOG.md

### Fixed

- [#1 Unfollow](https://gitlab.com/ngerakines/tavern/-/issues/1)
- [#2 Display note](https://gitlab.com/ngerakines/tavern/-/issues/2)
- [#3 Display Announce](https://gitlab.com/ngerakines/tavern/-/issues/3)
- [#4 Announce from feed](https://gitlab.com/ngerakines/tavern/-/issues/4)
- [#5 Create note in existing conversation](https://gitlab.com/ngerakines/tavern/-/issues/5)
- [#6 Mention actor in note](https://gitlab.com/ngerakines/tavern/-/issues/6)
- [#7 Actor inbox - Undo follow](https://gitlab.com/ngerakines/tavern/-/issues/7)
- [#8 Activity resource](https://gitlab.com/ngerakines/tavern/-/issues/8)
- [#9 Actor outbox](https://gitlab.com/ngerakines/tavern/-/issues/9)
- [#10 Server stats](https://gitlab.com/ngerakines/tavern/-/issues/10)
- [#11 Create Dockerfile](https://gitlab.com/ngerakines/tavern/-/issues/11)
- [#12 Object resource](https://gitlab.com/ngerakines/tavern/-/issues/12)
- [#13 Tag resource](https://gitlab.com/ngerakines/tavern/-/issues/13)
- [#14 Implement user avatars (icon)](https://gitlab.com/ngerakines/tavern/-/issues/14)
- [#15 Display local and remote actor icons on feeds](https://gitlab.com/ngerakines/tavern/-/issues/15)
- [#16 View conversation](https://gitlab.com/ngerakines/tavern/-/issues/16)
- [#17 Display object images in feed](https://gitlab.com/ngerakines/tavern/-/issues/17)
- [#18 Create note with attached image](https://gitlab.com/ngerakines/tavern/-/issues/18)
- [#19 Asset storage](https://gitlab.com/ngerakines/tavern/-/issues/19)
- [#20 Make local storage for remote content configurable](https://gitlab.com/ngerakines/tavern/-/issues/20)
- [#22 Database migrations](https://gitlab.com/ngerakines/tavern/-/issues/22)
- [#23 Store webfinger subject field on actor record](https://gitlab.com/ngerakines/tavern/-/issues/23)
- [#25 Network tables should reference actors primary key](https://gitlab.com/ngerakines/tavern/-/issues/25)
- [#27 The keys table should reference actor](https://gitlab.com/ngerakines/tavern/-/issues/27)
- [#29 The object events table should reference the pk of the objects table.](https://gitlab.com/ngerakines/tavern/-/issues/29)
- [#30 Use postgres jsonb for payloads](https://gitlab.com/ngerakines/tavern/-/issues/30)
- [#37 Metrics registry](https://gitlab.com/ngerakines/tavern/-/issues/37)
- [#33 Markdown rendering](https://gitlab.com/ngerakines/tavern/-/issues/33)
- [#38 Delete note](https://gitlab.com/ngerakines/tavern/-/issues/38)
- [#40 Convert threading to use context instead of conversation](https://gitlab.com/ngerakines/tavern/-/issues/40)
- [#42 Remove object conversation and context tracking](https://gitlab.com/ngerakines/tavern/-/issues/42)

[Unreleased]: https://gitlab.com/ngerakines/tavern/-/compare/v0.2.1...master
[0.2.1]: https://gitlab.com/ngerakines/tavern/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/ngerakines/tavern/-/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/ngerakines/tavern/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/ngerakines/tavern/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/ngerakines/tavern/-/compare/v0.1.0-rc3...v0.1.0
[0.1.0-rc3]: https://gitlab.com/ngerakines/tavern/-/compare/v0.1.0-rc2...v0.1.0-rc3
[0.1.0-rc2]: https://gitlab.com/ngerakines/tavern/-/compare/v0.1.0-rc1...v0.1.0-rc2
[0.1.0-rc1]: https://gitlab.com/ngerakines/tavern/-/compare/0.0.1...v0.1.0-rc1