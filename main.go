package main

import (
	"log"
	"os"
	"sort"
	"time"

	"github.com/urfave/cli/v2"

	"github.com/ngerakines/tavern/asset"
	"github.com/ngerakines/tavern/g"
	"github.com/ngerakines/tavern/janitor"
	"github.com/ngerakines/tavern/json"
	"github.com/ngerakines/tavern/migrations"
	"github.com/ngerakines/tavern/publisher"
	"github.com/ngerakines/tavern/start"
	"github.com/ngerakines/tavern/web"

	_ "github.com/go-bindata/go-bindata"
)

var ReleaseCode string
var GitCommit string
var BuildTime string

func main() {
	compiledAt, err := time.Parse(time.RFC3339, BuildTime)
	if err != nil {
		compiledAt = time.Now()
	}
	if ReleaseCode == "" {
		ReleaseCode = "develop"
	}
	if GitCommit == "" {
		GitCommit = ""
	}

	g.BuildTime = BuildTime
	g.Release = ReleaseCode
	g.GitCommit = GitCommit

	app := cli.NewApp()
	app.Name = "tavern"
	app.Usage = "The tavern application."
	app.Version = g.Version()
	app.Compiled = compiledAt
	app.Copyright = "(c) 2020 Nick Gerakines"

	app.Commands = []*cli.Command{
		&start.InitAdminCommand,
		&start.InitServiceCommand,
		&web.Command,
		&asset.Command,
		&migrations.Command,
		&json.DebugJSONLDCommand,
		&publisher.Command,
		&janitor.SkeletonCommand,
		&janitor.CopyDatabaseCommand,
	}

	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
