# Federation

This document is meant to be a reference for all the ActivityPub federation-related behavior that Tavern implements. This includes every user action that can trigger an Activity, as well as every Object that is create, federated, or stored.

## Documentation conventions

Activity and object pairs are formatted as `Activity/Object`. For example:

* `Create/Note`: a `Create` activity pertaining to a `Note` object.
* `Follow/Person`: a `Follow` activity pertaining to a `Person` object.

# Supported Activities and Objects

| Activity | Object | Object |
|:---------|:-------|:-------|
| Create   | Note   |        |
| Announce | Note   |        |
| Follow   | Actor  |        |
| Undo     | Follow | Actor  |
| Accept   | Follow | Actor  |
| Reject   | Follow | Actor  |
| Follow   | Note   |        |
| Undo     | Follow | Note   |
| Accept   | Follow | Note   |
| Reject   | Follow | Note   |
| Follow   | Group  |        |
| Undo     | Follow | Group  |


# Data Retention

When activity is received by Tavern through activity federation, a determination is made as to if the activity is relevant to any users. Relevant activity and corresponding objects and actor references, along with their meta-data, are stored locally.

# Users and Actors

A user is a first-class actor and activities are a result of direct and indirect actions. Actors may generate activity immediately, but activity creation and federation may also be differed. An example would be sending a status update immediately, versus scheduling a status update to be published at a later time.

User discovery occurs through `GET /.well-known/webfinger` requests.

## Network

Tavern makes every effort to acknowledge follow activity through `Accept` and `Reject` responses. Users may configure "auto-follow" to immediately accept follow requests. Alternatively, users have the ability to ignore, accept, or reject follow activities.

## Collections

Requests to the user outbox returns a paginated ordered list of objects that have been published to the default public location.

## Following and Unfollowing Actors

Tavern implements the following behavior for both inbound and outbound follow and unfollow actions:

To follow an actor, send a `Follow` activity with the object attribute set to the actor's inbox.

To unfollow an actor, send a `Undo` activity with the object being the previously sent `Follow` activity.

When a `Follow` activity is received, a `Accept` or `Reject` activity is expected. 

## Deleting Actors

Tavern implements the following behavior for inbound `Delete/Actor` activities:

When a server broadcasts a `Delete/Actor` activity, tavern will tombstone the local actor reference as well as all activities associated with the actor.

# Groups

A group is effectively a distribution list with roles.

Groups have followers but do not follow anyone.

## Joining a group

To join a group, an actor sends a `Follow/Group` activity to the group inbox.

Groups may be configured to auto accept followers.

### Configuration

Group owners may configure the default member role for new followers:

* Viewer (can receive announcements but cannot publish)
* Member (can receive announcements and publish activities)
* Owner (can receive announcements, publish activities, and manage the group)

Group owners can also configure the group to automatically accept new followers.

## Leaving a group

To leave a group, an actor sends a `Undo/Follow/Group` activity to the group inbox.

## Group change notifications

When an actor joins a group, a `Join` activity is created for the actor and group and published to all of the group's followers.

When an actor leaves a group, a `Leave` activity is created for the actor and group and published to all of the group's followers.

## Invitations

Persons may be invited to the group. When an `Invite/Person` activity is published to the group's inbox, the group will accept the next follow request from the invited actor.

```json
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "summary": "",
  "type": "Invite",
  "actor": "https://tavern.town/users/nick",
  "object": "https://tavern.town/groups/hiking",
  "target": "https://tavern.town/users/mason",
  "to": [
    "https://tavern.town/groups/hiking",
    "https://tavern.town/users/mason"
  ]
}
```

When the actor receives that activity, the actor (sender) and object (group) are cached, and then a `Follow/Group` activity sent to the group from the invited actor. The group that has received the invitation for the actor will respond to the invited actor with an `Accept/Follow` activity. 

## Publishing to a group

When an actor sends a `Create/Note` or `Announce/Note` activity to a group's inbox, the group will create an `Announce/Note` activity and publish it to all of the followers of the group.

Activities are published only to group followers, regardless of who the activity is sent to (to, cc, bto, or bcc). The public destination is not added to group announcements.

**Note**: Members must have either the "member" or "owner" role within the group to publish to the group, otherwise the activity will be denied as unauthorized.

### Limitations

Groups record their announcements and will not publish `Announce/Note` activities for objects more than once a day (configurable).

## Use Cases

### Public Group

A "public group" is a group that is configured to auto-accept new followers with the "member" role.

This is useful for:

* Providing

### Broadcast Group

A "broadcast group" is a group that has one central creator of activities, and many followers that are only viewers. New followers may be auto-accepted with the "viewer" role.

This is useful for:

* Providing "top down" updates for projects, teams, or organizations

### Working Group

A "working group" (or committee) is a group has a mix of both viewers and contributors. New followers may be auto-accepted with the "viewer" role.

This is useful for:

* Public project groups where discussions are visible, but contributions limited to a select number of individuals.

### Private Group

A "private group" is a group that is limited to contributors who are invited to the group. New followers are kept as "pending" until accepted.

# Public vs Private Activity

Public activities include the https://www.w3.org/ns/activitystreams#Public destination in the `to` and will always be the first element of that list.

Private activities will not include the above destination. It is presumed that all federated systems make best effort to respect privacy, however we encourage all Tavern users to acknowledge the inherent risk in using Activity Pub for private, secure, or otherwise confidential data transmission.

# Language

All content is presumed in English at this time. The `content` and `contentMap` attributes are set in activity.

# Notes

User "statuses" are created as "Note" activity objects.

* `Create/Note`
* `Announce/Note`

All notes are given a "context" attribute which is used to refer to the thread that the note is related to. Additionally, if the note is created as a "reply" to a specific object, the "inReplyTo" attribute is set to that object's ID (URL).

Created notes are public, by default, and federated to the inbox location of all acknowledged followers of the creator of the note. 

## Boosts (`Announce/Note`)

Tavern recognizes the common practice of "boosting" notes to increase the potential reach of that note.

Boosted notes are public, by default, with the activity federated to the inbox location of all acknowledged followers of the user boosting the object.

## Following and Unfollowing Notes

Tavern implements the following behavior for both inbound and outbound follow and unfollow actions:

To follow a note, send a `Follow` activity with the object attribute set to the object id of the note.

To unfollow a note, send a `Undo` activity with the object being the previously sent `Follow` activity.

When Tavern creates or receives a `Create/Note` or `Announce/Note` activity, it will attempt to forward the activity to all followers of as an inbox forwarding behavior.

**Important**: Tavern makes no effort to ensure the validity of the forwarded activity. It is up to recipient activity pub servers to verify the authenticity of forwarded activities and objects.

## Deleting Notes

When a `Delete/Note` activity is received, a tombstone is recorded for the activity.

When Tavern receives a `Delete/Note` activity, it will attempt to forward the activity to all followers of as an inbox forwarding behavior for the parent, if the note is a reply.

# Update

Update is not implemented at this time.

# Delete

Supported delete activity includes `Delete/Note` and `Delete/Actor`.

No other delete activities are implemented at this time.

# Add

Add is not implemented at this time.

# Remove

Remove is not implemented at this time.

# Like

Like is not implemented at this time.

# Undo

Supported undo activity includes `Undo/Follow` and `Undo/Follow/Note`.

No other Undo activity, including `Undo/Create` and `Undo/Announce` are implemented at this time.
